This is another small example. It shows:

- the levels of logging, depending on the type of information (info, error, raw, etc.)
- the VariantMap container

There are other useful containers in Urho3D and I'll complete this tutorial later. 

In order to run the example, there are a few steps to be undertaken:

1. Set the environment variable URHO3D_HOME. It must point to the folder where Urho3D is installed. In my case I put it into something like ~/opt/share/urho3d and avoided to install it as root.
2. Set up the folder (initialize). There are two things to do:
- call the script ../../common/init_folder.sh (note that the presence of the CoreData and the Data folders is not required in this tutorial)
- call the script ../../common/select_cmake.sh
3. Generate the Makefile by invoking "cmake ."
4. Compile the app with make
5. Run the app : ./bin/Environment_Settings

Hope this is useful! Enjoy creating fabulous computer games!