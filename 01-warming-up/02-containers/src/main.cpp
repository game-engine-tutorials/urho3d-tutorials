#include <Urho3D/Urho3DAll.h>

using namespace Urho3D;


int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());

	// First, let's set up a log
	Log log(context);
	log.SetLevel(LOG_TRACE);
	
	// Checking the log levels
	Log::Write(LOG_RAW, "This is a raw log\nThere is no timeStamp\nNewline tag is advisable\n---------------------\n");
	Log::Write(LOG_TRACE, "This is a trace log");
	Log::Write(LOG_DEBUG, "This is a debug log");
	Log::Write(LOG_INFO, "This is an info log");
	Log::Write(LOG_WARNING, "This is a warning log");
	Log::Write(LOG_ERROR, "This is an error log");
	Log::Write(LOG_NONE, "This is a none log. It should not be visible");


	// Let's build a hash map container. In Urho3d it is called VariantMap
	VariantMap container = { 
		{"country", "Australia"}, 
		{"capital", "Canberra"}, 
		{"population", 25690600},
		{"density_per_square_mile", 8.5f},
		{"right_side_driving", false}
	};

	String message;
	String key;

	// Now, we ce retrieve the values associated with the keys and log them to the terminal
	message = "";
	key = "country";
	message.Append(key).Append(" = ").Append(container[key].ToString());
	Log::Write(LOG_RAW, message);
	Log::Write(LOG_RAW, "\n");

	message = "";
	key = "capital";
	message.Append(key).Append(" = ").Append(container[key].ToString());
	Log::Write(LOG_RAW, message);
	Log::Write(LOG_RAW, "\n");
	
	message = "";
	key = "population";
	message.Append(key).Append(" = ").Append(container[key].ToString());
	Log::Write(LOG_RAW, message);
	Log::Write(LOG_RAW, "\n");

	message = "";
	key = "density_per_square_mile";
	message.Append(key).Append(" = ").Append(container[key].ToString());
	Log::Write(LOG_RAW, message);
	Log::Write(LOG_RAW, "\n");

	message = "";
	key = "right_side_driving";
	message.Append(key).Append(" = ").Append(container[key].ToString());
	Log::Write(LOG_RAW, message);
	Log::Write(LOG_RAW, "\n"); 

	// But wait, what if I want to iterate through the tontainer ?

	Log::Write(LOG_RAW, "\n\n"); 
	Log::Write(LOG_RAW, "\n\nIterating over the container"); 
	Log::Write(LOG_RAW, "----------------------------\n"); 

	for (auto&& item: container)
	{
		message = "";
		message.Append(item.first_.ToString()).Append(" = ").Append(item.second_.ToString());
		Log::Write(LOG_RAW, message);
		Log::Write(LOG_RAW, "\n");
	}

	// Seems we lost the keys. Let's try something different
	Log::Write(LOG_RAW, "\n\n"); 
	Log::Write(LOG_RAW, "\n\nIterating over the key set"); 
	Log::Write(LOG_RAW, "----------------------------\n"); 

	for (auto&& key: container.Keys()) {
		message = "";
		message.Append(key.ToString());
		Log::Write(LOG_RAW, message);
		Log::Write(LOG_RAW, "\n");
	}

	// Still no chance of getting the real keys. Let's iterate over the values directly
	Log::Write(LOG_RAW, "\n\n"); 
	Log::Write(LOG_RAW, "\n\nIterating over the value set"); 
	Log::Write(LOG_RAW, "----------------------------\n"); 

	for (auto&& value: container.Values()) {
		message = "";
		message.Append(value.ToString());
		Log::Write(LOG_RAW, message);
		Log::Write(LOG_RAW, "\n");
	}

	// It seems that the keys are converting into hash values, and we use a one-way function
	// When using VariantMap, younought to know what you put inside the box

	return 0; 
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}