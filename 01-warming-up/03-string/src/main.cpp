#include <Urho3D/Urho3DAll.h>
#include <iostream>
using namespace Urho3D;

int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());

	// First, let's set up a log
	Log log(context);
	log.SetLevel(LOG_TRACE);

	String message;

	message = "The first mass producer of cars has been ";
	String carMake = "Ford";
	message.Append(carMake);
	Log::Write(LOG_INFO, message);

	message = "The beginning of the 20th century saw the birth of ";
	String sameCarMake = carMake;
	message.Append(sameCarMake);
	Log::Write(LOG_INFO, message);

	message = "The Japanese flagship car company is ";
	String japaneseCarMake("Toyota");
	message.Append(japaneseCarMake);
	Log::Write(LOG_INFO, message);

	message = "Young people's faborite German car is ";
	String germanCarMake("Bayerische Motor Werke", 20);
	message.Append(germanCarMake);
	Log::Write(LOG_INFO, message);

	message = "If you want to convert miles in kilometers you need to multiply the value by ";
	String mileToKilometer(1.609f);
	message.Append(mileToKilometer);
	Log::Write(LOG_INFO, message);

	message = "Maximum legal speed in continental Europe is ";
	String maximumSpeedInKilometers(130);
	message.Append(mileToKilometer);
	Log::Write(LOG_INFO, message);

	message = "Mustang used to be known as ";
	message += "Shelby Cobra";
	Log::Write(LOG_INFO, message);

	message = "Bananas and oranges";
	String justOneFruit = message.Substring(0,7);
	Log::Write(LOG_INFO, justOneFruit);

	message = "         and this is the best part of the book     ";
	String noLeadingOrTrailingSpaces = message.Trimmed();
	Log::Write(LOG_INFO, noLeadingOrTrailingSpaces);

	String firstName = "John";
	String lastName = "Deere";
	String capitalizedLastName = lastName.ToUpper();
	message = firstName + " " + capitalizedLastName;
	Log::Write(LOG_INFO, message);

	Vector<String> tokens = {"apples", "bananas", "cherries"};
	String allTheTokensInAString;
	allTheTokensInAString.Join(tokens, " AND ");
	Log::Write(LOG_INFO, allTheTokensInAString);

	// using standard console out class
	message = "This was an Urho3D String";
	std::cout << message.CString() << std::endl;

	String firstString = "";
	String secondString = "There is something about this string";
	std::cout << "the first string is" <<  (firstString.Empty() ? " empty " : " not empty ") << std::endl;
	std::cout << "the second string is" <<  (secondString.Empty() ? " empty " : " not empty ") << std::endl;

	return 0; 
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}