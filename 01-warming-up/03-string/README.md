This tutorial plays with the String class. A large portion of a game code deals with text. It is important to know how to use this class. Even the standard iostream framework is able to use Urho3D's String. 

Please refer to the first tutorial in the series for the general setup of the project. 