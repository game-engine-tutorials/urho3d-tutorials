#include "CameraPosition.h"

using namespace Urho3D;

CameraPosition CameraPosition::instance;
CameraPosition::CameraPosition() {
	updatePosition(Vector3(0.0f,0.0f, 0.0f));
}

CameraPosition& CameraPosition::getInstance() {
	return CameraPosition::instance;
}

float CameraPosition::calculateSquaredDistance(const Urho3D::Vector3& otherObject) {

	float squaredDistance = (cameraPosition.x_ - otherObject.x_)*(cameraPosition.x_ - otherObject.x_) +
							(cameraPosition.y_ - otherObject.y_)*(cameraPosition.y_ - otherObject.y_) +
							(cameraPosition.z_ - otherObject.z_)*(cameraPosition.z_ - otherObject.z_);
	return squaredDistance;
}

void CameraPosition::updatePosition(const Urho3D::Vector3& newPosition) {
	cameraPosition = newPosition;
}