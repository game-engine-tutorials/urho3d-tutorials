#include <Urho3D/Scene/Scene.h>

#include "Sensor.h"
#include "CameraPosition.h"

#include <Urho3D/DebugNew.h>

using namespace Urho3D;

const float FIVE_UNITS = 5.0f;
const float FIVE_UNITS_SQUARED = FIVE_UNITS * FIVE_UNITS;


Sensor::Sensor(Context* context) :
    LogicComponent(context)
{
    SetUpdateEventMask(USE_UPDATE);
}

// This paranoia sensor detects if the camera is less than 5 distance units from the cube
// It is advisable to avoid heavy math operations, like extracting a square root. 
// All we need is to know if the distance is close enough.
void Sensor::Update(float timeStep)
{
    float distance = CameraPosition::getInstance().calculateSquaredDistance(node_->GetPosition());
    if (  distance < FIVE_UNITS_SQUARED) {
    	//Choose a random position at ground level (2nd value is 0.5f)
    	node_->SetPosition(Vector3(Random(100.0f) - 50.0f, 0.5f, Random(100.0f) - 50.0f));
    }
}