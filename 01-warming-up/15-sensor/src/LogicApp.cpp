// This is based on sample no. 4 from Urho source tree
//
#include "LogicApp.h"
#include "Sensor.h"
#include "CameraPosition.h"


#include <iostream>

using namespace Urho3D;
using namespace std;

const unsigned STONE = 1;
const unsigned TERRAIN = 2;
const unsigned STONE_ALT = 3;

LogicApp::LogicApp(Context* context)
: Application(context) {
	errorsDetected = false; 
    // This enables later the call to Node::CreateComponent
    context->RegisterFactory<Sensor>();

}

void LogicApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Try to get close enough to the cube (if you can) - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void LogicApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(LogicApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void LogicApp::Stop() {
	engine_->DumpResources(true);
}

void LogicApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void LogicApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(LogicApp, HandleUpdate));
}

void LogicApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();
    moveCamera(timeStep);

}

void LogicApp::createScene()
{
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();
    createCube(Vector3(0.0f, 0.5f, 0.0f));
}

void LogicApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();
  
    planeNode = scene_->CreateChild("Earth");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
    floorType = STONE;
}

void LogicApp::createLightSource() {
    lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
}

Node* LogicApp::createCube(Vector3 position)  {
    auto* cache = GetSubsystem<ResourceCache>();

    Node* boxNode = scene_->CreateChild("Box");
    boxNode->SetPosition(position);
    boxNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));

    auto* boxObject = boxNode->CreateComponent<StaticModel>();
    boxObject->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    boxObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
    auto* sensor = boxNode->CreateComponent<Sensor>();

    return boxNode;
}


void LogicApp::createCamera() {
    yaw_ = -90.0f;   // horizontal angle
    pitch_ = 30.0f; // vertical angle

  
    cameraNode_ = scene_->CreateChild("Hunter");
    cameraNode_->CreateComponent<Camera>();

    cameraNode_->SetPosition(Vector3(10.0f, 10.0f, 0.0f));
    CameraPosition::getInstance().updatePosition(cameraNode_->GetPosition());
}

void LogicApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move. \nIf the cube disappears, it means it has moved elsewhere. Find it again.");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void LogicApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void LogicApp::moveCamera(float timeStep)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);


    CameraPosition::getInstance().updatePosition(cameraNode_->GetPosition());

}





    