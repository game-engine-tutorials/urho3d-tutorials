This tutorial is based on sample no. 5 and it shows how to add logic to the nodes. You can create your own classes derivedfrom LogicComponent and associate them to a Node.

In our case, the box has a sensor. If you get too close to it, the sensor will detect the proximity and trigger a teleport. Then, you have to look for the box and try to get close to it.

This simple mechanism could be used in a game to detect if a user is near an object. It is easy to add a sphere and a cone to the scene and see what happens when you get close.

A node can have more than one Logic Component, so be creative and add something else to our box. Maybe a color change.

The CameraPosition class has a global instance. A typical game will always have some mechanism for sharing important data to all the objects it is made out of.


Happy coding!