#pragma once

#include <Urho3D/Urho3DAll.h>

class TextureManager;
class PieceManager;

class LogicApp : public Urho3D::Application {
	URHO3D_OBJECT(LogicApp, Urho3D::Application);
public:
	explicit LogicApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    
    /// Subscribe to application-wide logic update events.
    void SubscribeToEvents();
    /// Handle the logic update event.
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSource();
    void createCamera();
    void createInstructions();
    Urho3D::Node* createCube(Urho3D::Vector3 position);
    void setupViewport();
    void moveCamera(float timeStep);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;
    Urho3D::SharedPtr<Node> lightNode;
    unsigned floorType;

};