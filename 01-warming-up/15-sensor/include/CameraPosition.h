#pragma once
#include <Urho3D/Urho3DAll.h>



class CameraPosition {
public:
	static CameraPosition& getInstance();
	float calculateSquaredDistance(const Urho3D::Vector3& otherObject);
	void updatePosition(const Urho3D::Vector3& newPosition);
private:
	CameraPosition();
	static CameraPosition instance;
	Urho3D::Vector3 cameraPosition;


};

