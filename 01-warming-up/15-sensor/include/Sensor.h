#pragma once

#include <Urho3D/Urho3DAll.h>

class Sensor : public Urho3D::LogicComponent
{
    URHO3D_OBJECT(Sensor, Urho3D::LogicComponent);

public:
    /// Construct.
    explicit Sensor(Urho3D::Context* context);

    /// Handle scene update. Called by LogicComponent base class.
    void Update(float timeStep) override;

};