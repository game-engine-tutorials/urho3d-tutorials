The FileSystem class from Urho is useful when it comes to file ops. This tutorial is exploring the members of the class. There is a handful of things one can do on a file system:

- navigate and scan the folders
- create/rename/delete files
- interrogate the system about a specific file

