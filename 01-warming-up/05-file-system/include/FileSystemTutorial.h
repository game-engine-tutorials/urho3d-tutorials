#pragma once
#include <Urho3D/Urho3DAll.h>

class FileSystemTutorial
{
public:
	FileSystemTutorial(Urho3D::SharedPtr<Urho3D::Context> context);
	void printDirInfo();
	void execDirOps();
private:
	Urho3D::SharedPtr<Urho3D::Context> context;
	Urho3D::SharedPtr<Urho3D::Log> log;
	Urho3D::SharedPtr<Urho3D::FileSystem> fileSystem;

};

