#include "FileSystemTutorial.h"

using namespace Urho3D;

FileSystemTutorial::FileSystemTutorial(SharedPtr<Context> context)
{
	this->context = context;
	log = new Log(this->context);
	fileSystem = new FileSystem(this->context);
}

void FileSystemTutorial::printDirInfo()
{
	String message;

	message = "Current dir = ";
	message.Append(fileSystem->GetCurrentDir());
	URHO3D_LOGINFO(message);


	message = "Program dir = ";
	message.Append(fileSystem->GetProgramDir());
	URHO3D_LOGINFO(message);

	message = "User doc dir = ";
	message.Append(fileSystem->GetUserDocumentsDir());
	URHO3D_LOGINFO(message);
}

void FileSystemTutorial::execDirOps()
{
	String message;

	URHO3D_LOGINFO("Check if there is an executable file named FileSystem in the bin folder ...");
	String programDir = fileSystem->GetProgramDir();
	String filePath = programDir;
	filePath.Append("FileSystem");
	if (fileSystem->FileExists(filePath))
		URHO3D_LOGINFO("... there is a file named FileSystem in the bin folder");
	else
		URHO3D_LOGINFO("... the bin folder does not contain a file named FileSystem");

	
}