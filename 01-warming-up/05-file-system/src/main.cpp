#include <Urho3D/Urho3DAll.h>
#include "FileSystemTutorial.h"


void printDirInfo() 
{

}

int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	FileSystemTutorial *fileSystemTutorial = new FileSystemTutorial(context);

	fileSystemTutorial->printDirInfo();
	fileSystemTutorial->execDirOps();

	delete fileSystemTutorial;
	return 0; 
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}

