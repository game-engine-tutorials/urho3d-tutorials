This tutorial is heavily based on sample no. 4 from the Urho3D source tree. I simplified it, by removing the mushrooms.

There is a floor that covers a big chunk of ground.

Initially the camera is placed high above ground level, tilted to the left and and towards the floor. You can change that either with the arrow keys or with mouse.

The floor can be made of two types of stone. You have the choice of a terrain type. Feel free to change the floor by pressing 1, 2, or 3. While the forw	ard key can move the viewer beyound the plane of the floor, in a normal game you would need to implement some collision check. So far, the Translation of the camera is not controlled by hard limits.

This tutorial is the base for the next ones. You will be able to play with the light. Until then ...

Happy coding !
