// This is based on sample no. 4 from Urho source tree
//
#include "FloorApp.h"


#include <iostream>

using namespace Urho3D;
using namespace std;

const unsigned STONE = 1;
const unsigned TERRAIN = 2;
const unsigned STONE_ALT = 3;

FloorApp::FloorApp(Context* context)
: Application(context) {
	errorsDetected = false; 
}

void FloorApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Display a square floor - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void FloorApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(FloorApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createScene();
    createInstructions();
    setupViewport();

    // Hook up to the frame update events
    SubscribeToEvents();
}

void FloorApp::Stop() {
	engine_->DumpResources(true);
}

void FloorApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void FloorApp::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(FloorApp, HandleUpdate));
}

void FloorApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

    // Move the camera, scale movement with time step
    moveCamera(timeStep);

}

void FloorApp::createScene()
{
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);

    // Create the Octree component to the scene. This is required before adding any drawable components, or else nothing will
    // show up. The default octree volume will be from (-1000, -1000, -1000) to (1000, 1000, 1000) in world coordinates; it
    // is also legal to place objects outside the volume but their visibility can then not be checked in a hierarchically
    // optimizing manner
    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();
}

void FloorApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();
    // Create a child scene node (at world origin) and a StaticModel component into it. Set the StaticModel to show a simple
    // plane mesh with a "stone" material. Note that naming the scene nodes is optional. Scale the scene node larger
    // (100 x 100 world units)
    planeNode = scene_->CreateChild("Earth");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
    floorType = STONE;
}

void FloorApp::createLightSource() {
    lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
}

void FloorApp::createCamera() {
    // The camera looks  at a 60 deg angle towards the floor and is tilted 30 deg to the left (thus the corner of the square)
    yaw_ = 30.0f;   // horizontal angle
    pitch_ = 60.0f; // vertical angle

    // Create a scene node for the camera, which we will move around
    // The camera will use default settings (1000 far clip distance, 45 degrees FOV, set aspect ratio automatically)
    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();

    // Set an initial position for the camera 100 units above the plane
    cameraNode_->SetPosition(Vector3(0.0f, 100.0f, 0.0f));
}

void FloorApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move. \nPress 1, 2, or 3 in order to switch floor texture.");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void FloorApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void FloorApp::moveCamera(float timeStep)
{
    // Do not move if the UI has a focused element (the console)
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    // Movement speed as world units per second
    const float MOVE_SPEED = 20.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

    // the numbers 1 to 3 change the texture of the ground floor 
    if (input->GetKeyDown(KEY_1))
        setFloorType(STONE);
    if (input->GetKeyDown(KEY_2))
        setFloorType(TERRAIN);
    if (input->GetKeyDown(KEY_3))
        setFloorType(STONE_ALT);
}


void FloorApp::setFloorType(unsigned type) {
    if ( floorType != type) {
        auto* cache = GetSubsystem<ResourceCache>();
        switch(type) {
            case STONE:
                cout << "Set floor type to stone" << endl;
                planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
                floorType = type;
                break;
            case TERRAIN:
                cout << "Set floor type to terrain" << endl;
                planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
                floorType = type;
                break;
            case STONE_ALT:
                cout << "Set floor type to stone alternate" << endl;
                planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiledH.xml"));
                floorType = type;
                break;
            default:
                break;
        }
    } 
    
}


    