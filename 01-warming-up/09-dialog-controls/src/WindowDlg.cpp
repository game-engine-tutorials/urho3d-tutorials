#include "WindowDlg.h"

using namespace Urho3D;

WindowDlg::WindowDlg(Context* context)
: Window(context)
{


}

void WindowDlg::initWindow(HorizontalAlignment horizontalAlign, VerticalAlignment verticalAlign, String caption)
{
    
	SetMinWidth(450);
    SetLayout(LM_VERTICAL,10); //, 6, IntRect(6, 6, 6, 6));
    SetAlignment(horizontalAlign, verticalAlign);
    SetName("Window");
    SetStyleAuto();

    addTitleBar(caption);
    
}


void WindowDlg::addWindowTitle(String caption) {
    windowTitle = new Text(context_);
    titleBar->AddChild(windowTitle);
    windowTitle->SetName(caption); // this is an interesting idea
    windowTitle->SetText(caption);
    windowTitle->SetStyleAuto();

    
}

// 1. new Element
// 2. add to the parent element
// 3. do the stuff, including styling
void WindowDlg::addButtonClose() 
{
    buttonClose = new Button(context_);
    titleBar->AddChild(buttonClose);    
    buttonClose->SetName("The Button that closes the app");
    buttonClose->SetStyle("CloseButton");
}

void WindowDlg::addTitleBar(String caption) 
{
    titleBar = new UIElement(context_);
    AddChild(titleBar);
    titleBar->SetMinSize(0, 24);
    titleBar->SetVerticalAlignment(VA_TOP);
    titleBar->SetLayoutMode(LM_HORIZONTAL);   

    addWindowTitle(caption);
    addButtonClose();

}

void WindowDlg::addCheckBox()
{
    auto* checkBox = new CheckBox(context_);
    AddChild(checkBox);
    checkBox->SetName("CheckBox");
    checkBox->SetStyleAuto();
    
}
void WindowDlg::addButton()
{
    auto* button = new Button(context_);
    AddChild(button);

    button->SetName("Button");
    button->SetMinHeight(24);
    button->SetStyleAuto();
}

void WindowDlg::addLineEdit()
{
     auto* lineEdit = new LineEdit(context_);
     AddChild(lineEdit);
    lineEdit->SetName("LineEdit");
    lineEdit->SetMinHeight(24);
    lineEdit->SetStyleAuto();
    
}

void WindowDlg::addOptionalCheckBoxes()
{
    lineOne = new UIElement(context_);
    AddChild(lineOne);
    lineOne->SetStyleAuto();
    lineOne->SetLayout(LM_HORIZONTAL,10);

    auto* checkBoxA = new CheckBox(context_);
    lineOne->AddChild(checkBoxA);
    checkBoxA->SetStyleAuto();
    checkBoxA->SetName("CheckBoxA");

    auto* textA = new Text(context_);
    lineOne->AddChild(textA);
    textA->SetName("With Butter"); // this is an interesting idea
    textA->SetText("Butter");
    textA->SetStyleAuto();

    auto* checkBoxB = new CheckBox(context_);
    lineOne->AddChild(checkBoxB);
    checkBoxB->SetStyleAuto();
    checkBoxB->SetName("CheckBoxB");

    auto* textB = new Text(context_);
    lineOne->AddChild(textB);
    textB->SetName("With Honey"); // this is an interesting idea
    textB->SetText("Honey");
    textB->SetStyleAuto();
}

void WindowDlg::addYesNoRadios()
{
    // Alas, it seems there is no such thing as a radio button 
}

void WindowDlg::initControls()
{
    // Create a CheckBox
    addCheckBox();
    addButton();
    addLineEdit();
    addOptionalCheckBoxes();
    addYesNoRadios();
    


    //this->SetStyleAuto();
    
}
