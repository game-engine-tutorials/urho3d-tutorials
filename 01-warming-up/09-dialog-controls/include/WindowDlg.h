#pragma once

#include <Urho3D/Urho3DAll.h>


class WindowDlg : public Urho3D::Window
{
public:
	WindowDlg(Urho3D::Context* context);
	void initWindow(Urho3D::HorizontalAlignment horizontalAlign, 
					Urho3D::VerticalAlignment verticalAlign, 
					Urho3D::String caption = "Window dialog");
	void initControls();
private:
	void addTitleBar(String caption);
	void addWindowTitle(String caption);
	void addButtonClose();
	void addCheckBox();
	void addButton();
	void addLineEdit();
	void addOptionalCheckBoxes();
	void addYesNoRadios();

	Urho3D::SharedPtr<Urho3D::Text> windowTitle;
	Urho3D::SharedPtr<Urho3D::UIElement> titleBar;
	Urho3D::SharedPtr<Urho3D::Button> buttonClose;

	Urho3D::SharedPtr<Urho3D::UIElement> lineOne;
	Urho3D::SharedPtr<Urho3D::UIElement> lineTwo;

};


