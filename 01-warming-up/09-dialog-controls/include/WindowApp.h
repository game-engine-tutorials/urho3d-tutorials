#pragma once

#include <Urho3D/Urho3DAll.h>

class WindowDlg;

class WindowApp : public Urho3D::Application
{
	URHO3D_OBJECT(WindowApp, Urho3D::Application);
public:
	explicit WindowApp(Urho3D::Context* context);
private:
    void Setup() override;
    void Start() override;
    void Stop() override;
    
    void HandleKeyUp(StringHash eventType, VariantMap& eventData);

    bool errorsDetected;
    Urho3D::SharedPtr<WindowDlg> windowDlg;
    Urho3D::SharedPtr<Urho3D::UIElement> uiRoot_;
};



