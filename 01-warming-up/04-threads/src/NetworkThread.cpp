#include <iostream>
#include <thread>
#include <chrono>

#include "NetworkThread.h"

using namespace Urho3D;

void NetworkThread::ThreadFunction() {
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Log log(context);
	log.SetLevel(LOG_TRACE);

	int networkCongestionFactor = 0;

	for ( int counter = 0; counter < 50; counter++) // I know, postfix ++ is considered bad programming
	{
		//Log::Write(LOG_RAW, "Ping sent to www.yahoo.com ..."); // This is a bad idea. The log doesn't flush
		std::cout << "Ping sent to www.yahoo.com ..." << std::endl;
		std::this_thread::sleep_for(std::chrono::microseconds(15));
		std::this_thread::sleep_for(std::chrono::microseconds(5* (networkCongestionFactor%7)));

		networkCongestionFactor++;
	}

	Stop();
}
