#include <Urho3D/Urho3DAll.h>
#include "GraphicThread.h"
#include "NetworkThread.h"
#include <thread>
#include <chrono>



int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());

	GraphicThread graphicThread;
	NetworkThread networkThread;


	graphicThread.Run();
	networkThread.Run();

	std::this_thread::sleep_for(std::chrono::microseconds(10));

	while ( graphicThread.IsStarted() || networkThread.IsStarted()) {
		std::this_thread::sleep_for(std::chrono::microseconds(10));
	}

	return 0; 
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}

