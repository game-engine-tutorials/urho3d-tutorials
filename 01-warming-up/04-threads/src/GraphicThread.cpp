#include <iostream>
#include <thread>
#include <chrono>
#include "GraphicThread.h"

using namespace Urho3D;

void GraphicThread::ThreadFunction() {
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());

	// Could do it in the constructor, too
	//Log log(context);
	//log.SetLevel(LOG_TRACE);

	for ( int counter = 0; counter < 100; counter++) // I know, postfix ++ is considered bad programming
	{
		//Log::Write(LOG_RAW, "Updating frame"); // Bad idea
		std::cout << "Updating frame" << std::endl;
		//usleep(10000); // 25 fps 
		std::this_thread::sleep_for(std::chrono::microseconds(10));
	}

	Stop();
}
