#pragma once

 #include <Urho3D/Urho3DAll.h>

class NetworkThread : public Thread
{
protected:
	void ThreadFunction() override;
};