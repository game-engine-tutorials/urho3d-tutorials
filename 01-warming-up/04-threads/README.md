You can see here two background threads competing against each other. The main thread is waiting for the end of both worker threads. Note that I use std::cout instead of the Log class. The Log does some buffering and is flushing here and there. There is much to learn.

Note that the ThreadFunction calls the Stop method. If you don't do it, the respective thread will hang forever.