This example shows how to send events (messages) from one object to another. The notification mechanism is far from being trivial.

One event is sent to all the subscribers. This is why I used the owner string to check. If you disable the owner == name condition, you will see messages in double. This illustrates one fact: an event could be sent to everyone (broadcast) or to an individual.

The names of the people in this tutorial are purely fictious.