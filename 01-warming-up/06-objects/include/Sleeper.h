#pragma once
#include <Urho3D/Urho3DAll.h>

class Sleeper : public Urho3D::Object
{
	URHO3D_OBJECT(Sleeper, Object);
public:
	Sleeper(Urho3D::Context *context, String name);

	void handleRing(StringHash eventType, VariantMap& eventData);
private:
	void snooze();

	String name;
	int ringCount;
	
};