#pragma once
#include <Urho3D/Urho3DAll.h>


URHO3D_EVENT(SOE_SNOOZE, SOESnooze)
{
    
}

URHO3D_EVENT(SOE_RING, SOE_Ring)
{
    
}

class Phone : public Urho3D::Object
{
	URHO3D_OBJECT(Phone, Object);
public:
	Phone(Urho3D::Context *context, String phone, String owner);

	void handleSnooze(StringHash eventType, VariantMap& eventData);
	void testRingAlarm();

private:
	Urho3D::String phoneName;
	Urho3D::String ownerName;

	
};
