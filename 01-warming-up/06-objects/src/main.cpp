#include <Urho3D/Urho3DAll.h>
#include "Phone.h"
#include "Sleeper.h"

using namespace Update;

int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Phone *samsung = new Phone(context, "Samsung Galaxy", "Jonathan Ive");
	Phone *iphone = new Phone(context, "iPhone 5S", "Bill Gates");

	Sleeper *jive = new Sleeper(context, "Jonathan Ive");
	Sleeper *bill = new Sleeper(context, "Bill Gates");
	Sleeper *linus = new Sleeper(context, "Linus Torvalds");

	samsung->testRingAlarm();
	iphone->testRingAlarm();
	samsung->testRingAlarm();
	samsung->testRingAlarm();

	return 0; 
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}