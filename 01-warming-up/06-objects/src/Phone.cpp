#include "Phone.h"
#include <iostream>
using namespace Urho3D;


Phone::Phone(Urho3D::Context *context, String phone, String owner)
: Object(context)
{
	this->phoneName = phone;
	this->ownerName = owner;

	SubscribeToEvent(SOE_SNOOZE, URHO3D_HANDLER(Phone, handleSnooze));
}

void Phone::handleSnooze(StringHash eventType, VariantMap& eventData) {
	// using direct console logging
	String eventOwner = eventData["Owner"].ToString();
	if (eventOwner == ownerName) 
	{
		String action = eventData["Action"].ToString();
		std::cout <<  action.CString() <<  std::endl;
	}
	
}

void Phone::testRingAlarm()
{
	VariantMap eventData;
	eventData["Phone"] = phoneName;
	eventData["Owner"] = ownerName;
	SendEvent(SOE_RING, eventData);
}


