#include "Phone.h"
#include "Sleeper.h"

#include <iostream>
using namespace Urho3D;


Sleeper::Sleeper(Urho3D::Context *context, String name)
: Object(context)
{
	this->name = name;
	ringCount = 2;
	SubscribeToEvent(SOE_RING, URHO3D_HANDLER(Sleeper, handleRing));
}

void Sleeper::handleRing(StringHash eventType, VariantMap& eventData) {
	// using direct console logging
	String phone = eventData["Phone"].ToString();
	String owner = eventData["Owner"].ToString();

	if (owner.Compare(name) == 0) 
	{
		std::cout << "The " << phone.CString() << " is ringing" << std::endl;
		snooze();
	}

}

void Sleeper::snooze()
{
	VariantMap eventData;
	eventData["Owner"] = name;
	switch (ringCount)
	{
		case 2: 
			eventData["Action"] = name + " pressed the snooze button"; 

			ringCount --; 
			SendEvent(SOE_SNOOZE, eventData);
			return ;
		case 1: 
			eventData["Action"] = name + " has thrown the phone and the device is now broken"; 
			ringCount --; 
			SendEvent(SOE_SNOOZE, eventData);
			break;
		case 0:
		default: 
			eventData["Action"] = name + " is asleep and cannot hear a broken phone";
			SendEvent(SOE_SNOOZE, eventData); 
			break;
	}
	
}