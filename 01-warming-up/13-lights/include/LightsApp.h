#pragma once

#include <Urho3D/Urho3DAll.h>


class LightsApp : public Urho3D::Application {
	URHO3D_OBJECT(LightsApp, Urho3D::Application);
public:
	explicit LightsApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();

    // Lights ... 
    void createPointLightSources();
    void createSpotLightSources();
    Urho3D::Node* createPointLightSource(Color color, Vector3 position);
    Urho3D::Node* createSpotLightSource(Color color, Vector3 position);

    // ... camera ...
    void createCamera();
    void createInstructions();
    void setupViewport();

    // ... action
    void moveCamera(float timeStep);
    void switchLight(Urho3D::Node* lightNode, bool& flag);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    // Better to keep tabs on the objects of the scene
    Urho3D::SharedPtr<Urho3D::Node> planeNode;
    Urho3D::SharedPtr<Urho3D::StaticModel> planeObject;

    // Behind every light there is a node
    // These are the center and the four corner lights
    Urho3D::SharedPtr<Urho3D::Node> lightNodeCenter;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeSouth;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeNorth;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeWest;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeEast;
    
    // point lights
    Urho3D::SharedPtr<Urho3D::Node> lightNodeRed;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeBlue;

    // spot lights
    Urho3D::SharedPtr<Urho3D::Node> lightNodeYellow;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeGreen;
    Urho3D::SharedPtr<Urho3D::Node> lightNodeWhite;

    // flags indicating if a light is on or off
    bool redFlag;
    bool blueFlag;
    bool yellowFlag;
    bool greenFlag;
    bool whiteFlag;

    // Dirty trick for handling key ghosting
    int timeCount;



};