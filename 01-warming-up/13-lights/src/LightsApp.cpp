// This is based on sample no. 4 from Urho source tree
//
#include "LightsApp.h"


#include <iostream>

using namespace Urho3D;
using namespace std;

LightsApp::LightsApp(Context* context)
: Application(context) {
	errorsDetected = false; 
    timeCount = 0;
}

void LightsApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Display a floor illuminated by several lights - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void LightsApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(LightsApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createScene();
    createInstructions();
    setupViewport();

    // Hook up to the frame update events
    SubscribeToEvents();
}

void LightsApp::Stop() {
	engine_->DumpResources(true);
}

void LightsApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void LightsApp::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(LightsApp, HandleUpdate));
}

void LightsApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

    // Move the camera, scale movement with time step
    moveCamera(timeStep);

}

void LightsApp::createScene()
{
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();

    createPlane();
    createPointLightSources();
    createSpotLightSources();
    createCamera();
}

void LightsApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();
    planeNode = scene_->CreateChild("Moon");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
    
}

void LightsApp::createPointLightSources() {

    // The square area stretches between -50.0f and 50.0f in both directions. These are the center and  the corners.
    lightNodeCenter = createPointLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(0.0f, 5.0f, 0.0f));
    lightNodeSouth = createPointLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(-45.0f, 5.0f, -45.0f));
    lightNodeNorth = createPointLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(-45.0f, 5.0f, 45.0f));
    lightNodeWest = createPointLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(45.0f, 5.0f, -45.0f));
    lightNodeEast = createPointLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(45.0f, 5.0f, 45.0f));

    // Our red light
    redFlag = true;
    lightNodeRed = createPointLightSource(Color(1.0f, 0.0f, 0.0f), Vector3(30.0f, 3.0f, 30.0f));

    // ... and the blue one
    blueFlag = true;
    lightNodeBlue = createPointLightSource(Color(0.0f, 0.0f, 1.0f), Vector3(30.0f, 3.0f, -30.0f));

}

// A light is always attached to a node. The physical position of the light is that of the node
Node* LightsApp::createPointLightSource(Color color, Vector3 position) {
    Node* lightNode = scene_->CreateChild("PointLight");
    lightNode->SetPosition(position);
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_POINT);
    light->SetRange(10.0f);
    light->SetColor(color);
    light->SetBrightness(1.0f);
    return lightNode;
}

void LightsApp::createSpotLightSources() {
    yellowFlag = true;
    lightNodeYellow = createSpotLightSource(Color(1.0f, 1.0f, 0.0f), Vector3(-30.0f, 3.0f, -30.0f));

    greenFlag = true;
    lightNodeGreen = createSpotLightSource(Color(0.0f, 1.0f, 0.0f), Vector3(-20.0f, 3.0f, -20.0f));

    whiteFlag = true;
    lightNodeWhite = createSpotLightSource(Color(1.0f, 1.0f, 1.0f), Vector3(-10.0f, 3.0f, -10.0f));
}

Node* LightsApp::createSpotLightSource(Color color, Vector3 position) {
    auto* cache = GetSubsystem<ResourceCache>();
    Node* lightNode = scene_->CreateChild("PointLight");
    lightNode->SetDirection(Vector3(Sin(0.0f), -1.5f, Cos(0.0f)));
    lightNode->SetPosition(position);
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_SPOT);
    light->SetRange(90.0f);
    light->SetRampTexture(cache->GetResource<Texture2D>("Textures/RampExtreme.png"));
    light->SetFov(35.0f);
    light->SetColor(color);
    light->SetSpecularIntensity(5.0f);
    light->SetCastShadows(true);
    light->SetShadowBias(BiasParameters(0.00005f, 0.0f));
    return lightNode;
}

void LightsApp::createCamera() {
    yaw_ = 30.0f;   // horizontal angle
    pitch_ = 70.0f; // vertical angle

    
    cameraNode_ = scene_->CreateChild("Operator behind camera");
    cameraNode_->CreateComponent<Camera>();

    cameraNode_->SetPosition(Vector3(0.0f, 100.0f, 0.0f));


}

void LightsApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move. \nPress 0 or 1 to turn off / on a group of lights.\nPress 2, 3, 4, 5, 6   in order to switch off/on individual lights.");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void LightsApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void LightsApp::moveCamera(float timeStep)
{
    if (timeCount > 0)
        timeCount --;

    // Do not move if the UI has a focused element (the console)
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    // Movement speed as world units per second
    const float MOVE_SPEED = 20.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

    // This trick avoids calling multiple times. So far I don't know how to check if a key is ghost pressed
    if (timeCount == 0 ){

        if (input->GetKeyDown(KEY_0)) {
            if (redFlag)  switchLight(lightNodeRed, redFlag);
            if (blueFlag)  switchLight(lightNodeBlue, blueFlag);
            if (yellowFlag)  switchLight(lightNodeYellow, yellowFlag);
            if (greenFlag)  switchLight(lightNodeGreen, greenFlag);
            if (whiteFlag)  switchLight(lightNodeWhite, whiteFlag);

        } 
        else if (input->GetKeyDown(KEY_1)){
            if (!redFlag)  switchLight(lightNodeRed, redFlag);
            if (!blueFlag)  switchLight(lightNodeBlue, blueFlag);
            if (!yellowFlag)  switchLight(lightNodeYellow, yellowFlag);
            if (!greenFlag)  switchLight(lightNodeGreen, greenFlag);
            if (!whiteFlag)  switchLight(lightNodeWhite, whiteFlag);
        } 
        else if (input->GetKeyDown(KEY_2))
                switchLight(lightNodeRed, redFlag);
        else if (input->GetKeyDown(KEY_3))
            switchLight(lightNodeBlue, blueFlag);
        else if (input->GetKeyDown(KEY_4))
            switchLight(lightNodeYellow, yellowFlag);
        else if (input->GetKeyDown(KEY_5))
            switchLight(lightNodeGreen, greenFlag);
        else if (input->GetKeyDown(KEY_6))
            switchLight(lightNodeWhite, whiteFlag);
    }

}


void LightsApp::switchLight(Node* lightNode, bool& flag) {
    
        auto* cache = GetSubsystem<ResourceCache>();
        if (flag)
            scene_->RemoveChild(lightNode);
        else
            scene_->AddChild(lightNode);
        flag = !flag;
        
        timeCount = 30;
    
    
}



    