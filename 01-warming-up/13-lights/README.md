This tutorial is loosely based on sample no. 4 from the Urho3d source tree. The ground (floor) is that grey stone sheet.

The previous example used a diffuse light. There are other types of light in Urho:

- spot light
- point light

Both allow a precise illumination of only a small area. You can control many parameters:

- the elevation of the  lightsource
- its intensity (brightness)
- its color

Also, it is possible to turn it off and on ny simply adding / removing the node associated with the light source.

This example allows you to navigate in a 3D space. There are 5 colored lights (well one is white). Additionally, there are light sources in the center and the corners of the grey square. You can turn on or off the colored lights:

- as a group, by pressing 0 for none) or 1 for all
- individually, by pressing the numbers 2 to 6  

While the example seems to be complex, I tried to structure it in a way that simplifies its understanding. Note how a light is turned on and off.