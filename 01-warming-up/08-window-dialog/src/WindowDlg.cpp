#include "WindowDlg.h"

using namespace Urho3D;

WindowDlg::WindowDlg(Context* context)
: Window(context)
{


}

void WindowDlg::initWindow(HorizontalAlignment horizontalAlign, VerticalAlignment verticalAlign, String caption, bool close)
{
	SetMinWidth(250);
    SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
    SetAlignment(horizontalAlign, verticalAlign);
    SetName("Window");

	auto* windowTitle = new Text(context_);
    windowTitle->SetName(caption); // this is an interesting idea
    windowTitle->SetText(caption);

    auto* titleBar = new UIElement(context_);
    titleBar->SetMinSize(0, 24);
    titleBar->SetVerticalAlignment(VA_TOP);
    titleBar->SetLayoutMode(LM_HORIZONTAL);    
    titleBar->AddChild(windowTitle);




    // Add the title bar to the Window
    AddChild(titleBar);

    // Apply styles
    SetStyleAuto();
    windowTitle->SetStyleAuto();
    
    if (close)
    {
        auto* buttonClose = new Button(context_);
	    buttonClose->SetName("CloseButton");
	    titleBar->AddChild(buttonClose);	
	    buttonClose->SetStyle("CloseButton");
    }
}



