
#include <iostream>
#include "WindowApp.h"
#include "WindowDlg.h"

using namespace Urho3D;

WindowApp::WindowApp(Context* context) 
	:	Application(context),
        uiRoot_(GetSubsystem<UI>()->GetRoot())
{
	errorsDetected = false;
}

void WindowApp::Setup()
{
    engineParameters_[EP_WINDOW_TITLE] = "Window with controls - press ESC to close"; // setting here the title of the main window
    engineParameters_[EP_FULL_SCREEN]  = false; // this tutorial has to show a window. The real game would normally be launched in full screen mode

    if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void WindowApp::Start()
{
    if (errorsDetected)
        return;

// Enable OS cursor
    GetSubsystem<Input>()->SetMouseVisible(true);

    // Load XML file containing default UI style sheet
    auto* cache = GetSubsystem<ResourceCache>();
    auto* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");

    // Set the loaded style as default style
    uiRoot_->SetDefaultStyle(style);


    SharedPtr<WindowDlg> tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_LEFT, VA_TOP, "Left Top");
    windowDlg[0][0] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_LEFT, VA_CENTER, "Left Center");
    windowDlg[0][1] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_LEFT, VA_BOTTOM, "Left Bottom");
    windowDlg[0][2] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_CENTER, VA_TOP, "Center Top", true);
    windowDlg[1][0] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_CENTER, VA_CENTER, "Center Center", true);
    windowDlg[1][1] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_CENTER, VA_BOTTOM, "Center Bottom");
    windowDlg[1][2] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_RIGHT, VA_TOP, "Right Top", true);
    windowDlg[2][0] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_RIGHT, VA_CENTER, "Right Center");
    windowDlg[2][1] = tempWindowDlg;

    tempWindowDlg = new WindowDlg(context_);
    uiRoot_->AddChild(tempWindowDlg);
    tempWindowDlg->initWindow(HA_RIGHT, VA_BOTTOM, "Right Bottom", true);
    windowDlg[2][2] = tempWindowDlg;


	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(WindowApp, HandleKeyUp));
}

void WindowApp::Stop()
{
    engine_->DumpResources(true);
}

void WindowApp::HandleKeyUp(StringHash eventType, VariantMap& eventData)
{
    using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}
