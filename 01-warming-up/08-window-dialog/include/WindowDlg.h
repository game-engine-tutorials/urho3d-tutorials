#pragma once

#include <Urho3D/Urho3DAll.h>


class WindowDlg : public Urho3D::Window
{
public:
	WindowDlg(Urho3D::Context* context);
	void initWindow(Urho3D::HorizontalAlignment horizontalAlign, 
					Urho3D::VerticalAlignment verticalAlign, 
					Urho3D::String caption = "Window dialog", 
					bool close = false);
private:


};


