This tutorial is heavily based on the Sample.h/Sample.inl and the first two examples of the Urho3D distribution.

For sake of usefulness, I've simplified the code as much as possible. There is a black window and if you press ESC it will close.

Note that you have to set an environment variable so that it points to the CoreData, resp the Data folders from the original source tree of Urho3D. You will know which variable.

The nine dialog boxes are placed in a 3x3 grid/matrix. The example shows how to pass parameters to a dialog class and set its place on screen. There will be another tutorial about how to ad controls to the dialog box.