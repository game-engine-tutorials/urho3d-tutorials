#include "TextureManager.h"
#include <iostream>

using namespace Urho3D;
using namespace std;

TextureManager::TextureManager(Urho3D::Context* context) 
: Object(context) {

}


bool TextureManager::load() {
	bool success = true;
	if (success) success = loadTexture("ChessData/Board/DarkSquare.dds","dark-square");
	if (success) success = loadTexture("ChessData/Board/LightSquare.dds","light-square");

	if (success) success = loadTexture("ChessData/Pieces/Black/BlackKing.dds","black-king");
	if (success) success = loadTexture("ChessData/Pieces/Black/BlackQueen.dds","black-queen");
	if (success) success = loadTexture("ChessData/Pieces/Black/BlackRook.dds","black-rook");
	if (success) success = loadTexture("ChessData/Pieces/Black/BlackBishop.dds","black-bishop");
	if (success) success = loadTexture("ChessData/Pieces/Black/BlackKnight.dds","black-knight");
	if (success) success = loadTexture("ChessData/Pieces/Black/BlackPawn.dds","black-pawn");

	if (success) success = loadTexture("ChessData/Pieces/White/WhiteKing.dds","white-king");
	if (success) success = loadTexture("ChessData/Pieces/White/WhiteQueen.dds","white-queen");
	if (success) success = loadTexture("ChessData/Pieces/White/WhiteRook.dds","white-rook");
	if (success) success = loadTexture("ChessData/Pieces/White/WhiteBishop.dds","white-bishop");
	if (success) success = loadTexture("ChessData/Pieces/White/WhiteKnight.dds","white-knight");
	if (success) success = loadTexture("ChessData/Pieces/White/WhitePawn.dds","white-pawn");

	return success;
}

bool TextureManager::loadTexture(Urho3D::String path, Urho3D::String key) {
	auto* cache = GetSubsystem<ResourceCache>();
	auto* texture = cache->GetResource<Texture2D>(path);
	if (!texture) {
		std::cerr 	<< std::endl 
    				<< "Cannot load square texture" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	return false;
	}
	textures[key] = texture;
	return true;
}

SharedPtr<Texture2D> TextureManager::operator[](String key) {
	if ( !textures.Contains(key))
		return nullptr;
	return textures[key];
}