#include "ChessApp.h"
#include "TextureManager.h"
#include "PieceManager.h"

#include <iostream>



using namespace Urho3D;
using namespace std;

// The screen is a 4x4 matrix. top rop will contain 3 fixed horses
// a horse will slide vertically on the 3rd column
// a horse will change its size in the big 3x3 square below top row


static const unsigned NUM_ROWS = 8;
static const unsigned NUM_COLUMNS = 8;

static const StringHash VAR_VELOCITY("Velocity");

const int FILE_A = 0;
const int FILE_B = 1;
const int FILE_C = 2;
const int FILE_D = 3;
const int FILE_E = 4;
const int FILE_F = 5;
const int FILE_G = 6;
const int FILE_H = 7;

const int ROW_1 = 7;
const int ROW_2 = 6;
const int ROW_3 = 5;
const int ROW_4 = 4;
const int ROW_5 = 3;
const int ROW_6 = 2;
const int ROW_7 = 1;
const int ROW_8 = 0;

const int _A1 = 11;
const int _A2 = 12;
const int _A3 = 13;
const int _A4 = 14;
const int _A5 = 15;
const int _A6 = 16;
const int _A7 = 17;
const int _A8 = 18;

const int _B1 = 21;
const int _B2 = 22;
const int _B3 = 23;
const int _B4 = 24;
const int _B5 = 25;
const int _B6 = 26;
const int _B7 = 27;
const int _B8 = 28;

const int _C1 = 31;
const int _C2 = 32;
const int _C3 = 33;
const int _C4 = 34;
const int _C5 = 35;
const int _C6 = 36;
const int _C7 = 37;
const int _C8 = 38;

const int _D1 = 41;
const int _D2 = 42;
const int _D3 = 43;
const int _D4 = 44;
const int _D5 = 45;
const int _D6 = 46;
const int _D7 = 47;
const int _D8 = 48;

const int _E1 = 51;
const int _E2 = 52;
const int _E3 = 53;
const int _E4 = 54;
const int _E5 = 55;
const int _E6 = 56;
const int _E7 = 57;
const int _E8 = 58;

const int _F1 = 61;
const int _F2 = 62;
const int _F3 = 63;
const int _F4 = 64;
const int _F5 = 65;
const int _F6 = 66;
const int _F7 = 67;
const int _F8 = 68;

const int _G1 = 71;
const int _G2 = 72;
const int _G3 = 73;
const int _G4 = 74;
const int _G5 = 75;
const int _G6 = 76;
const int _G7 = 77;
const int _G8 = 78;

const int _H1 = 81;
const int _H2 = 82;
const int _H3 = 83;
const int _H4 = 84;
const int _H5 = 85;
const int _H6 = 86;
const int _H7 = 87;
const int _H8 = 88;

ChessApp::ChessApp(Context* context)
: Application(context) {
	errorsDetected = false;
}


void ChessApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Display a chess board - press ESC to close"; // setting here the title of the main window
    engineParameters_[EP_FULL_SCREEN]  = false; // this tutorial has to show a window. The real game would normally be launched in full screen mode

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void ChessApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(ChessApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createBoard();
    createPieces();
	



    // Hook up to the frame update events
    SubscribeToEvents();
}

void ChessApp::Stop() {
	engine_->DumpResources(true);
}

void ChessApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}


void ChessApp::createBoard() {
	auto* cache = GetSubsystem<ResourceCache>();
    auto* graphics = GetSubsystem<Graphics>();
    auto* ui = GetSubsystem<UI>();

    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();
    float squareSize = width > height ? (height / NUM_ROWS) : (width / NUM_ROWS);
    tileSize = squareSize;

    textureManager = new TextureManager(context_);
    if (!textureManager->load()){
        std::cerr   << std::endl 
                    << "Cannot load textures" 
                    << std::endl 
                    << "The program will halt soon"
                    << std::endl;
        errorsDetected = true;
        engine_->Exit(); // graceful shutdown
    }

    pieceManager = new PieceManager(context_, textureManager);
    pieceManager->setPieceSize(squareSize,squareSize);

    // A board is 8 columns of 8 squares each
    for ( int column = 0; column < NUM_COLUMNS; column++) {
    	Vector<SharedPtr<Sprite> > boardColumn;
    	tiles.Push(boardColumn);
    }

    // The board
    for ( int column = 0; column < NUM_COLUMNS; column++) {
    	for (int row = 0; row < NUM_ROWS; row++) {
    		Vector<SharedPtr<Sprite> > & boardColumn = tiles[column];
            SharedPtr<Sprite> tileSquare = pieceManager->create (((column+row) % 2 == 0) ? "light-square" : "dark-square");
			float xPos = column * squareSize + squareSize / 2;
			float yPos = row * squareSize +  squareSize / 2;
			tileSquare->SetPosition(Vector2(xPos, yPos));
			ui->GetRoot()->AddChild(tileSquare);
			boardColumn.Push(tileSquare);
		}
    }



}

void ChessApp::createPieces() {
    createPawns();
    createRooks();
    createKnights();
    createBishops();
    createQueens();
    createKings();
}

void ChessApp::createPawns() {
    // The 16 pawns (8 each side)
    for ( int column = 0; column < NUM_COLUMNS; column++) {
        SharedPtr<Sprite> whitePawn = pieceManager->create ("white-pawn");
        tiles[column][ROW_2]->AddChild(whitePawn);
        pieces.Push(whitePawn);

        SharedPtr<Sprite> blackPawn = pieceManager->create ("black-pawn");
        tiles[column][ROW_7]->AddChild(blackPawn);
        pieces.Push(blackPawn);
    }
}

void ChessApp::createRooks() {

    // The 4 rooks (one pair each side)
    SharedPtr<Sprite> whiteRook = pieceManager->create ("white-rook");
    getTile(_A1)->AddChild(whiteRook);
    pieces.Push(whiteRook);

    whiteRook = pieceManager->create ("white-rook");
    getTile(_H1)->AddChild(whiteRook);
    pieces.Push(whiteRook);

    SharedPtr<Sprite> blackRook = pieceManager->create ("black-rook");
    getTile(_A8)->AddChild(blackRook);
    pieces.Push(blackRook);

    blackRook = pieceManager->create ("black-rook");
    getTile(_H8)->AddChild(blackRook);
    pieces.Push(blackRook);
}

void ChessApp::createKnights() {
    // The knights (one pair each side)
    SharedPtr<Sprite> whiteKnight = pieceManager->create ("white-knight");
    getTile(_B1)->AddChild(whiteKnight);
    pieces.Push(whiteKnight);

    whiteKnight = pieceManager->create ("white-knight");
    getTile(_G1)->AddChild(whiteKnight);
    pieces.Push(whiteKnight);

    SharedPtr<Sprite> blackKnight = pieceManager->create ("black-knight");
    getTile(_B8)->AddChild(blackKnight);
    pieces.Push(blackKnight);

    blackKnight = pieceManager->create ("black-knight");
    getTile(_G8)->AddChild(blackKnight);
    pieces.Push(blackKnight);
}

void ChessApp::createBishops() {
    // The bishops (one pair each side)
    SharedPtr<Sprite> whiteBishop = pieceManager->create ("white-bishop");
    getTile(_C1)->AddChild(whiteBishop);
    pieces.Push(whiteBishop);

    whiteBishop = pieceManager->create ("white-bishop");
    getTile(_F1)->AddChild(whiteBishop);
    pieces.Push(whiteBishop);

    SharedPtr<Sprite> blackBishop = pieceManager->create ("black-bishop");
    getTile(_C8)->AddChild(blackBishop);
    pieces.Push(blackBishop);

    blackBishop = pieceManager->create ("black-bishop");
    getTile(_F8)->AddChild(blackBishop);
    pieces.Push(blackBishop);
}

void ChessApp::createQueens() {
    // The queens
    SharedPtr<Sprite> whiteQueen = pieceManager->create ("white-queen");
    getTile(_D1)->AddChild(whiteQueen);
    pieces.Push(whiteQueen);

    SharedPtr<Sprite> blackQueen = pieceManager->create ("black-queen");
    getTile(_D8)->AddChild(blackQueen);
    pieces.Push(blackQueen);
}

void ChessApp::createKings() {
    // The kings
    SharedPtr<Sprite> whiteKing = pieceManager->create ("white-king");
    getTile(_E1)->AddChild(whiteKing);
    pieces.Push(whiteKing);

    SharedPtr<Sprite> blackKing = pieceManager->create ("black-king");
    getTile(_E8)->AddChild(blackKing);
    pieces.Push(blackKing); 
}


void ChessApp::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ChessApp, HandleUpdate));
}

void ChessApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

    // Move sprites, scale movement with time step
    //updateHorses(timeStep);
}

SharedPtr<Sprite> ChessApp::getTile(int code) {
    int columnCode = code/10;
    int rowCode = code % 10;

    int row = 7-rowCode + 1;
    int col = columnCode -1;

    cout << "code " << code << " translates to (" << col << "," << row << ")" << endl;

    return tiles[col][row];
}