#include "PieceManager.h"
#include "TextureManager.h"

#include <iostream>

using namespace Urho3D;
using namespace std;

PieceManager::PieceManager(Urho3D::Context* context,SharedPtr<TextureManager> textureManager) 
: Object(context) {
	this->textureManager = textureManager;
}

void PieceManager::setPieceSize(float width, float height) {
	this->width = width;
	this->height = height;
}

SharedPtr<Sprite> PieceManager::create(String pieceType) {
	SharedPtr<Texture2D> texture = (*textureManager)[pieceType];
	if ( !texture)
		return nullptr;

	SharedPtr<Sprite> piece(new Sprite(context_));
	piece->SetTexture( texture);

	float xPos = width / 2;
	float yPos = height / 2;
	piece->SetHotSpot(IntVector2(width / 2, height / 2));
	piece->SetPosition(Vector2(xPos, yPos));
	piece->SetSize(IntVector2(width, height));
	piece->SetScale(1.0f);
	return piece;
}