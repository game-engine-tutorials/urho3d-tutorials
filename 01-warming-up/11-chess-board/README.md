This is a more elaborate example of how to draw a chess board. Note that in Urho you don't normally draw lines. The 64 squares are individual Sprites, as are the chess pieces. You can think of this as the next level of tutorial no. 10.

The pieces are supposed to be transparent, but for some reason, they are not. Anyway, the goal of this tutorial is to draw the chessboard with all the 32 pieces.

Each piece is a child of the respective tile (square). Moving a knight, for example, implies to remove the piece from its initial tile  and reattach to the destination tile.