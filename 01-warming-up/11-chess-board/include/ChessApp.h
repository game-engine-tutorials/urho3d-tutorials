#pragma once

#include <Urho3D/Urho3DAll.h>

class TextureManager;
class PieceManager;

class ChessApp : public Urho3D::Application {
	URHO3D_OBJECT(ChessApp, Urho3D::Application);
public:
	explicit ChessApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    
    /// Subscribe to application-wide logic update events.
    void SubscribeToEvents();
    /// Handle the logic update event.
    void HandleUpdate(StringHash eventType, VariantMap& eventData);

    void createBoard();
    void createPieces();
    void createPawns();
    void createRooks();
    void createKnights();
    void createBishops();
    void createQueens();
    void createKings();

    Urho3D::SharedPtr<Urho3D::Sprite> getTile(int code);

    bool errorsDetected;

    Urho3D::SharedPtr<Urho3D::Texture2D> darkSquareDDS;
    Urho3D::SharedPtr<Urho3D::Texture2D> lightSquareDDS;

    Urho3D::Vector<Urho3D::Vector<Urho3D::SharedPtr<Urho3D::Sprite> > > tiles;
    Urho3D::Vector<Urho3D::SharedPtr<Urho3D::Sprite> > pieces;
    Urho3D::SharedPtr<TextureManager> textureManager;
    Urho3D::SharedPtr<PieceManager> pieceManager;
    float tileSize;



};