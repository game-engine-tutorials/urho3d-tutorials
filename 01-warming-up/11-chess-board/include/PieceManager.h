#pragma once

#include <Urho3D/Urho3DAll.h>

class TextureManager;

class PieceManager : public Urho3D::Object {
	URHO3D_OBJECT(PieceManager, Urho3D::Object);
public:
	PieceManager(Urho3D::Context* context, Urho3D::SharedPtr<TextureManager> textureManager);
	void setPieceSize(float width, float height);
	Urho3D::SharedPtr<Urho3D::Sprite> create(Urho3D::String pieceType);
	 
private:

	Urho3D::SharedPtr<TextureManager> textureManager;
	float width;
	float height;
};