#pragma once

#include <Urho3D/Urho3DAll.h>


class TextureManager : public Urho3D::Object {
	URHO3D_OBJECT(TextureManager, Urho3D::Object);
public:
	TextureManager(Urho3D::Context* context);
	bool load();
	Urho3D::SharedPtr<Urho3D::Texture2D> operator[](Urho3D::String key);
private:
	bool loadTexture(Urho3D::String path, Urho3D::String key);

	Urho3D::HashMap<Urho3D::String, Urho3D::SharedPtr<Urho3D::Texture2D> > textures;
};

