#pragma once

#include <Urho3D/Urho3DAll.h>

class BaukastenApp : public Urho3D::Application {
	URHO3D_OBJECT(BaukastenApp, Urho3D::Application);
public:
	explicit BaukastenApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSource();
    void createCamera();
    void createInstructions();
    void createBaukasten();

    void createBasicBuildingBlocks();
    void createCube(Vector3 position);
    void createCone(Vector3 position);
    void createCylinder(Vector3 position);
    void createSphere(Vector3 position);

    void createCastle();
    void createSmallTower(float xPos, float yPos);
    void createBigTower(float xPos, float yPos);
    void createWall(float xPos, float yPos);

    void setupViewport();
    void moveCamera(float timeStep);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;
    Urho3D::SharedPtr<Node> lightNode;

};