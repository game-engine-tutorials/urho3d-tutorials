This is based on thee sample no. 5

This tutorial shows how to use a bunch of building blocks (baukasten) :

- cube
- cone
- cylinder
- sphere

Note how simple it is to combine them into more complex forms. The castle from this example is made of the for elements mentioned above, yet it looks like a toy castle.

The original sample had many cubes rotating. I feel that one can break down such complex behavior and progress in smaller steps.

Some drawbacks:

- so far I don't know how to scale up the elements (models).
- I have no idea how to apply colors on the elements, unles I use light. This tutorial avoids lights for sake of simplicity.
- there is no collision detection. One can "walk" through walls.

Happy coding!

