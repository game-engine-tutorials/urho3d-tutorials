// This is based on sample no. 5 from Urho source tree
//
#include "BaukastenApp.h"


#include <iostream>

using namespace Urho3D;
using namespace std;



BaukastenApp::BaukastenApp(Context* context)
: Application(context) {
	errorsDetected = false; 
}

void BaukastenApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Display some Baukasten building blocks - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void BaukastenApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(BaukastenApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void BaukastenApp::Stop() {
	engine_->DumpResources(true);
}

void BaukastenApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void BaukastenApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(BaukastenApp, HandleUpdate));
}

void BaukastenApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    moveCamera(timeStep);

}

void BaukastenApp::createScene()
{
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();

    createBaukasten();
}

void BaukastenApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();

    planeNode = scene_->CreateChild("Ground");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void BaukastenApp::createLightSource() {
    lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
}

void BaukastenApp::createCamera() {
    yaw_ = 20.0f;   // horizontal angle
    pitch_ = 10.0f; // vertical angle

    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 3.0f,-4.0f));
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
}

void BaukastenApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move. ");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void BaukastenApp::createBasicBuildingBlocks() {
    createCube(Vector3(0.0f, 1.0f, 0.0f));
    createCone(Vector3(1.0f, 1.0f, 0.0f));
    createCylinder(Vector3(2.0f, 1.0f, 0.0f));
    createSphere(Vector3(3.0f, 1.0f, 0.0f));
}


void BaukastenApp::createCube(Vector3 position)  {
    auto* cache = GetSubsystem<ResourceCache>();

    Node* boxNode = scene_->CreateChild("Box");
    boxNode->SetPosition(position);
    boxNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
    auto* boxObject = boxNode->CreateComponent<StaticModel>();
    boxObject->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    boxObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
}

void BaukastenApp::createCone(Vector3 position)  {
    auto* cache = GetSubsystem<ResourceCache>();

    Node* coneNode = scene_->CreateChild("Cone");
    coneNode->SetPosition(position);
    coneNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
    auto* coneObject = coneNode->CreateComponent<StaticModel>();
    coneObject->SetModel(cache->GetResource<Model>("Models/Cone.mdl"));
    coneObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void BaukastenApp::createCylinder(Vector3 position)  {
    auto* cache = GetSubsystem<ResourceCache>();

    Node* cylinderNode = scene_->CreateChild("Cylinder");
    cylinderNode->SetPosition(position);
    cylinderNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
    auto* cylinderObject = cylinderNode->CreateComponent<StaticModel>();
    cylinderObject->SetModel(cache->GetResource<Model>("Models/Cylinder.mdl"));
    cylinderObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));
}

void BaukastenApp::createSphere(Vector3 position)  {
    auto* cache = GetSubsystem<ResourceCache>();

    Node* sphereNode = scene_->CreateChild("Cylinder");
    sphereNode->SetPosition(position);
    sphereNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
    auto* sphereObject = sphereNode->CreateComponent<StaticModel>();
    sphereObject->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
    sphereObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}




void BaukastenApp::createCastle() {
    createSmallTower(5.0f, 14.0f);

    createWall(5.0f, 13.0f);
    createWall(5.0f, 12.0f);

    createBigTower(5.0f, 11.0f);

    createWall(5.0f, 10.0f);
    createWall(5.0f, 9.0f);

    createBigTower(5.0f, 8.0f);

    createWall(5.0f, 7.0f);
    createWall(5.0f, 6.0f);
    
    createSmallTower(5.0f, 5.0f);

    createWall(6.0f, 5.0f);
    createWall(7.0f, 5.0f);

    createSmallTower(8.0f, 5.0f);

    createWall(8.0f, 6.0f);
    createWall(8.0f, 7.0f);

    createSmallTower(8.0f, 8.0f);

    createWall(8.0f, 9.0f);
    createWall(8.0f, 10.0f);

    createSmallTower(8.0f, 11.0f);

    createWall(8.0f, 12.0f);
    createWall(8.0f, 13.0f);

    createSmallTower(8.0f, 14.0f);

    createWall(7.0f, 14.0f);
    createWall(6.0f, 14.0f);


}

void BaukastenApp::createSmallTower(float xPos, float yPos) {
    createCylinder(Vector3(xPos, 1.0f, yPos));
    createCylinder(Vector3(xPos, 2.0f, yPos));
    createCylinder(Vector3(xPos, 3.0f, yPos));

    createCone(Vector3(xPos, 4.0f, yPos));
}

void BaukastenApp::createWall(float xPos, float yPos) {
    createCube(Vector3(xPos, 1.0f, yPos));
    createCube(Vector3(xPos, 2.0f, yPos));
}

void BaukastenApp::createBigTower(float xPos, float yPos) {
    createCylinder(Vector3(xPos, 1.0f, yPos));
    createCylinder(Vector3(xPos, 2.0f, yPos));
    createCylinder(Vector3(xPos, 3.0f, yPos));
    createCylinder(Vector3(xPos, 4.0f, yPos));

    createSphere(Vector3(xPos, 5.0f, yPos));
}

void BaukastenApp::createBaukasten() {
    auto* cache = GetSubsystem<ResourceCache>();

    createBasicBuildingBlocks();

    createCastle();

    


    

    
}


void BaukastenApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void BaukastenApp::moveCamera(float timeStep)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

}





    