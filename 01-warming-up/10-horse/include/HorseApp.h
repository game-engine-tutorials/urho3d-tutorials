#pragma once

#include <Urho3D/Urho3DAll.h>

class HorseApp : public Urho3D::Application {
	URHO3D_OBJECT(HorseApp, Urho3D::Application);
public:
	explicit HorseApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    
    /// Subscribe to application-wide logic update events.
    void SubscribeToEvents();
    /// Handle the logic update event.
    void HandleUpdate(StringHash eventType, VariantMap& eventData);

	void createHorses();
    void createNonMovableHorse();
    void createColorChangingHorse();
    void createRotatingHorse();
    void createVerticalSlidingHorse();
    void createZoomingHorse();

	void updateHorses(float timeStep);
    void updateNonMovableHorse();
    void updateColorChangingHorse();
	void updateRotatingHorse(float timeStep);
	void updateVerticalSlidingHorse();
	void updateZoomingHorse();

	bool errorsDetected;

    Urho3D::SharedPtr<Urho3D::Texture2D> horseDDS;
    Urho3D::SharedPtr<Urho3D::Sprite> horseNonMovable;
    Urho3D::SharedPtr<Urho3D::Sprite> horseColorChanging;
    Urho3D::SharedPtr<Urho3D::Sprite> horseRotating;
    Urho3D::SharedPtr<Urho3D::Sprite> horseVerticalSliding;
    Urho3D::SharedPtr<Urho3D::Sprite> horseZooming;

    unsigned colorTicks;
    unsigned slidePosition;
    bool slideUp;

    unsigned zoomLevel;
    bool zoomIn;
    

};