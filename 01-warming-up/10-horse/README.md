This tutorial is based on the sample no. 3 from the Urho3D source tree. It shows how to load and display a DDS transparent texture. You can try with PNG images as well, and the results will depend on several factors:

- image itself
- the background

The original sample was doing many things. This example is more scholastic. It breaks down all the elements:

- display a horse at a given position
- change the color of a horse
- rotate a horse
- slide a horse vertically
- scale (zoom) a horse

The screen has been organized as a 4x4 matrix:

- the static horse is in the cell (0,0)
- the color changing horse is in the cell (1,0)
- the rotating horse is in the cell (2,0)
- the sliding horse moves on the fourth column between cells (3,0) and (3,3);
- the zooming horse occupies a 3x3 submatrix between cells (0,1) and (2,3)

The piublic domain image of a horse has been downloaded on the following site:

http://www.newdesignfile.com

If you consider that it is not a public domain image, please notify me and I will change it.