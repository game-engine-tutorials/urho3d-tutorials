#include "HorseApp.h"
#include <iostream>



using namespace Urho3D;

// The screen is a 4x4 matrix. top rop will contain 3 fixed horses
// a horse will slide vertically on the 3rd column
// a horse will change its size in the big 3x3 square below top row

enum HorseVariations {
	AT_REST = 0,
	COLOR_CHANGING = 1,
	ROTATING = 2,
	VERTICAL_LIFT = 3,
	SIZE_CHANGING = 4,
	HORSE_COUNT = SIZE_CHANGING + 1
};

static const unsigned NUM_SPRITES = 10;
static const unsigned NUM_ROWS = 4;
static const unsigned NUM_COLUMNS = 4;

static const StringHash VAR_VELOCITY("Velocity");

HorseApp::HorseApp(Context* context)
: Application(context) {
	errorsDetected = false;
}


void HorseApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Display a horse sprite - press ESC to close"; // setting here the title of the main window
    engineParameters_[EP_FULL_SCREEN]  = false; // this tutorial has to show a window. The real game would normally be launched in full screen mode

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void HorseApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(HorseApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());

    createHorses();

    // Hook up to the frame update events
    SubscribeToEvents();
}

void HorseApp::Stop() {
	engine_->DumpResources(true);
}

void HorseApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void HorseApp::createHorses()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* graphics = GetSubsystem<Graphics>();
    auto* ui = GetSubsystem<UI>();

    // Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

	horseDDS = cache->GetResource<Texture2D>("HorseData/StandingHorse.dds");
	if (!horseDDS){
    	std::cerr 	<< std::endl 
    				<< "Cannot load horse texture" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }

    createNonMovableHorse();
	createColorChangingHorse();
	createRotatingHorse();
	createVerticalSlidingHorse();
	createZoomingHorse();
 
}


void HorseApp::createNonMovableHorse() {
	auto* graphics = GetSubsystem<Graphics>();
	auto* ui = GetSubsystem<UI>();

	horseNonMovable = (new Sprite(context_));
    horseNonMovable->SetTexture(horseDDS);


	// Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;

    // middle of first cell on the first row
    float xPos = cellWidth / 2;
	float yPos = cellHeight / 2;

    horseNonMovable->SetHotSpot(IntVector2(cellWidth / 2, cellHeight / 2));
	horseNonMovable->SetPosition(Vector2(xPos, yPos));
	horseNonMovable->SetSize(IntVector2(cellWidth, cellHeight));
    horseNonMovable->SetScale(1.0f);

	ui->GetRoot()->AddChild(horseNonMovable);
}

void HorseApp::createColorChangingHorse() {
	auto* graphics = GetSubsystem<Graphics>();
	auto* ui = GetSubsystem<UI>();

	horseColorChanging = (new Sprite(context_));
    horseColorChanging->SetTexture(horseDDS);

	// Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;

    // middle of second cell on the first row
    float xPos = cellWidth * 3 / 2;
	float yPos = cellHeight / 2;

	horseColorChanging->SetHotSpot(IntVector2(cellWidth / 2, cellHeight / 2));
	horseColorChanging->SetPosition(Vector2(xPos, yPos));
	horseColorChanging->SetSize(IntVector2(cellWidth, cellHeight));
    horseColorChanging->SetScale(1.0f);

	colorTicks = 0;
	ui->GetRoot()->AddChild(horseColorChanging);
}

void HorseApp::createRotatingHorse() {
	auto* graphics = GetSubsystem<Graphics>();
	auto* ui = GetSubsystem<UI>();

	horseRotating = (new Sprite(context_));
    horseRotating->SetTexture(horseDDS);

	// Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;

    // middle of third cell on the first row
    float xPos = cellWidth * 5 / 2;
	float yPos = cellHeight / 2;

	horseRotating->SetHotSpot(IntVector2(cellWidth / 2, cellHeight / 2));
	horseRotating->SetPosition(Vector2(xPos, yPos));
	horseRotating->SetSize(IntVector2(cellWidth, cellHeight));
    horseRotating->SetScale(1.0f);

	colorTicks = 0;
	ui->GetRoot()->AddChild(horseRotating);
}


void HorseApp::createVerticalSlidingHorse() {
	auto* graphics = GetSubsystem<Graphics>();
	auto* ui = GetSubsystem<UI>();

	horseVerticalSliding = (new Sprite(context_));
    horseVerticalSliding->SetTexture(horseDDS);

	// Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;

    // middle of third cell on the first row
    float xPos = cellWidth * 7 / 2;
	float yPos = cellHeight / 2;

	horseVerticalSliding->SetHotSpot(IntVector2(cellWidth / 2, cellHeight / 2));
	horseVerticalSliding->SetPosition(Vector2(xPos, yPos));
	horseVerticalSliding->SetSize(IntVector2(cellWidth, cellHeight));
    horseVerticalSliding->SetScale(1.0f);

	slidePosition = 0;
	slideUp = false;
	ui->GetRoot()->AddChild(horseVerticalSliding);
}

void HorseApp::createZoomingHorse() {
	auto* graphics = GetSubsystem<Graphics>();
	auto* ui = GetSubsystem<UI>();

	horseZooming = (new Sprite(context_));
    horseZooming->SetTexture(horseDDS);

	// Get rendering window size as floats
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;

    // middle of third cell on the first row
    float xPos = cellWidth * 3 / 2;
	float yPos = cellHeight * 5 / 2; // because the first row is used

	horseZooming->SetHotSpot(IntVector2(cellWidth / 2, cellHeight / 2));
	horseZooming->SetPosition(Vector2(xPos, yPos));
	horseZooming->SetSize(IntVector2(cellWidth, cellHeight));
    horseZooming->SetScale(1.0f);

	zoomLevel = 100;
	zoomIn = true;
	ui->GetRoot()->AddChild(horseZooming);
}


void HorseApp::updateNonMovableHorse() {
	// Nothing to do here. It's a horse that does nothing, just static
}

void HorseApp::updateColorChangingHorse() {
	colorTicks ++;
	if ( colorTicks > 100) {
		colorTicks = 0;
		horseColorChanging->SetColor(Color(Random(0.8f) + 0.2f, Random(0.8f) + 0.2f, Random(0.8f) + 0.2f));

	}

}

void HorseApp::updateRotatingHorse(float timeStep) {
	// Slowly rotate the horse
	float newRot = horseRotating->GetRotation() + timeStep * 10.0f;
    horseRotating->SetRotation(newRot);
}

void HorseApp::updateVerticalSlidingHorse() {
	if ( slideUp ) {
		if (slidePosition == 0) {
			slideUp = false;
		}
	} else {
		if (slidePosition == 500){
			slideUp = true;
		}
	}

	if (slideUp)
		slidePosition --;
	else
		slidePosition++;

	auto* graphics = GetSubsystem<Graphics>();

	auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    float cellWidth = width / NUM_COLUMNS;
    float cellHeight = height / NUM_ROWS;
    float elevatorHeight = height - cellHeight;
    float elevatorPos = elevatorHeight * slidePosition / 500;

    // middle of third cell on the first row
    float xPos = cellWidth * 7 / 2;
	float yPos = cellHeight / 2 + elevatorPos;

	horseVerticalSliding->SetPosition(Vector2(xPos, yPos));
}

void HorseApp::updateZoomingHorse() {
if ( zoomIn ) {
		if (zoomLevel == 300) {
			zoomIn = false;
		}
	} else {
		if (zoomLevel == 50){
			zoomIn = true;
		}
	}

	if (zoomIn)
		zoomLevel ++;
	else
		zoomLevel --;

	float realZoomFactor = (float)zoomLevel / 100.0f;

	horseZooming->SetScale(realZoomFactor);
}

void HorseApp::updateHorses(float timeStep)
{
    auto* graphics = GetSubsystem<Graphics>();
    auto width = (float)graphics->GetWidth();
    auto height = (float)graphics->GetHeight();

    updateNonMovableHorse();
    updateColorChangingHorse();
    updateRotatingHorse(timeStep);
    updateVerticalSlidingHorse();
    updateZoomingHorse();

}

void HorseApp::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(HorseApp, HandleUpdate));
}

void HorseApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

    // Move sprites, scale movement with time step
    updateHorses(timeStep);
}