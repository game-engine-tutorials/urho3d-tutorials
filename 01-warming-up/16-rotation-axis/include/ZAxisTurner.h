#pragma once

#include <Urho3D/Urho3DAll.h>
#include "Turner.h"

class ZAxisTurner : public Turner 
{
    URHO3D_OBJECT(ZAxisTurner, Turner);
public:
    explicit ZAxisTurner(Urho3D::Context* context);
    void Update(float timeStep) override; 
};