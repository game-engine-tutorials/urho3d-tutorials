#pragma once

#include <Urho3D/Urho3DAll.h>

class RotationAxisApp : public Urho3D::Application {
	URHO3D_OBJECT(RotationAxisApp, Urho3D::Application);
public:
	explicit RotationAxisApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSource();
    void createCamera();
    void createInstructions();

    void createRotatingObjects();
    Urho3D::Node* createCube(Vector3 position);
    Urho3D::Node* createCone(Vector3 position);
    Urho3D::Node* createCylinder(Vector3 position);

    void setupViewport();
    void moveCamera(float timeStep);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;
    Urho3D::SharedPtr<Node> lightNode;

    Urho3D::SharedPtr<Node> cubeXRot;
    Urho3D::SharedPtr<Node> cubeYRot;
    Urho3D::SharedPtr<Node> cubeZRot;

    Urho3D::SharedPtr<Node> coneXRot;
    Urho3D::SharedPtr<Node> coneYRot;
    Urho3D::SharedPtr<Node> coneZRot;

    Urho3D::SharedPtr<Node> cylinderXRot;
    Urho3D::SharedPtr<Node> cylinderYRot;
    Urho3D::SharedPtr<Node> cylinderZRot;

    Urho3D::SharedPtr<Node> cubeXYZRot;
    
};