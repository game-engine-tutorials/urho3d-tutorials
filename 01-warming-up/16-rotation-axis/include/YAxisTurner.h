#pragma once

#include <Urho3D/Urho3DAll.h>
#include "Turner.h"

class YAxisTurner : public Turner 
{
    URHO3D_OBJECT(YAxisTurner, Turner);
public:
    explicit YAxisTurner(Urho3D::Context* context);
    void Update(float timeStep) override; 
};
