#pragma once

#include <Urho3D/Urho3DAll.h>
#include "Turner.h"

class XAxisTurner : public Turner 
{
    URHO3D_OBJECT(XAxisTurner, Turner);
public:
    explicit XAxisTurner(Urho3D::Context* context);
    void Update(float timeStep) override; 
};
