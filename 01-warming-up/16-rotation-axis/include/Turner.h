
#pragma once

#include <Urho3D/Urho3DAll.h>


/// Custom logic component for rotating a scene node.
class Turner : public Urho3D::LogicComponent
{
    URHO3D_OBJECT(Turner, Urho3D::LogicComponent);

public:
    /// Construct.
    explicit Turner(Urho3D::Context* context);

    /// Set rotation speed about the Euler axes. Will be scaled with scene update time step.
    void setRotationSpeed(const float speed);
    

    /// Return rotation speed.
    const float getRotationSpeed() const { return rotationSpeed_; }

private:
    /// Rotation speed.
    float rotationSpeed_;
};





