
#include "XAxisTurner.h"

using namespace Urho3D;


XAxisTurner::XAxisTurner(Context* context) :
    Turner(context)
{
    SetUpdateEventMask(USE_UPDATE);
}

void XAxisTurner::Update(float timeStep)
{
    node_->Rotate(Quaternion(getRotationSpeed() * timeStep, 0.0f, 0.0f));
}

