#include <iostream>
#include "YAxisTurner.h"

using namespace std;
using namespace Urho3D;

YAxisTurner::YAxisTurner(Context* context) :
    Turner(context)
{
    SetUpdateEventMask(USE_UPDATE);
}

void YAxisTurner::Update(float timeStep)
{
	cout << "YAxisTurner::Update" << endl;

    node_->Rotate(Quaternion(0.0f, getRotationSpeed() * timeStep, 0.0f));
}

