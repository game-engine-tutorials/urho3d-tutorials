#include <Urho3D/Urho3DAll.h>
#include "RotationAxisApp.h"


int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Urho3D::SharedPtr<RotationAxisApp> application(new RotationAxisApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}