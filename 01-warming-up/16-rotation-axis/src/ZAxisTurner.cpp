#include "ZAxisTurner.h"

using namespace Urho3D;

ZAxisTurner::ZAxisTurner(Context* context) :
    Turner(context)
{
    SetUpdateEventMask(USE_UPDATE);
}

void ZAxisTurner::Update(float timeStep)
{
    node_->Rotate(Quaternion(0.0f, 0.0f, getRotationSpeed() * timeStep));
}

