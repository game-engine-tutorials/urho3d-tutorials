
#include <iostream>
#include "WindowApp.h"
using namespace Urho3D;

WindowApp::WindowApp(Context* context) 
	:	Application(context)
{
	errorsDetected = false;
}

void WindowApp::Setup()
{
    engineParameters_[EP_WINDOW_TITLE] = "Basic window tutorial - press ESC to close"; // setting here the title of the main window
    engineParameters_[EP_FULL_SCREEN]  = false; // this tutorial has to show a window. The real game would normally be launched in full screen mode

    if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void WindowApp::Start()
{
 	if (!errorsDetected)
    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(WindowApp, HandleKeyUp));
}

void WindowApp::Stop()
{
    engine_->DumpResources(true);
}

void WindowApp::HandleKeyUp(StringHash eventType, VariantMap& eventData)
{
    using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}
