#pragma once

#include <Urho3D/Urho3DAll.h>

class WindowApp : public Urho3D::Application
{
	URHO3D_OBJECT(WindowApp, Urho3D::Application);
public:
	explicit WindowApp(Urho3D::Context* context);
private:
    void Setup() override;
    void Start() override;
    void Stop() override;
    
    void HandleKeyUp(StringHash eventType, VariantMap& eventData);

    bool errorsDetected;
};



