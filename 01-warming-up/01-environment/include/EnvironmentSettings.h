#pragma once

#include <Urho3D/Urho3DAll.h>

class EnvironmentSettings : public Urho3D::Application {
public:
	EnvironmentSettings(Urho3D::Context *context);
	/*virtual*/ void Setup() override;
	/*virtual*/ void Start() override;
	/*virtual*/	void Stop() override;
	
};

