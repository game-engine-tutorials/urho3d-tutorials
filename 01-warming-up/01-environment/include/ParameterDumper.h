#pragma once

#include <Urho3D/Urho3DAll.h>


class ParameterDumper {
public:
	ParameterDumper(const Urho3D::VariantMap& parameters, bool showType = false);
	void dumpEngineParameters();
private:
	void logParameter(Urho3D::String parameterName);
	String formatParameterInfo(Urho3D::String parameterName, const Urho3D::Variant& parameter, Urho3D::String type);

	const Urho3D::VariantMap& parameters_;
	bool showType_;
};
