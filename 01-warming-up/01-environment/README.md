This is a small example. It shows several things:

- the basic project structure
- a minimal application class that works
- how to log messages to the terminal (if the app is launched from the console)
- how to dump the parameters to the terminal console



There are a few steps to be undertaken:

1. Set the environment variable URHO3D_HOME. It must point to the folder where Urho3D is installed. In my case I put it into something like ~/opt/share/urho3d and avoided to install it as root.
2. Set up the folder (initialize). There are two things to do:
- call the script ../../common/init_folder.sh
- call the script ../../common/select_cmake.sh
3. Generate the Makefile by invoking "cmake ."
4. Compile the app with make
5. Run the app : ./bin/Environment_Settings

Hope this is useful! Enjoy creating fabulous computer games!