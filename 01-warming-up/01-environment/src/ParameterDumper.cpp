#include "ParameterDumper.h"

using namespace Urho3D;


ParameterDumper::ParameterDumper(const Urho3D::VariantMap& parameters, bool showType)
	: parameters_(parameters),
	  showType_(showType) {

}

void ParameterDumper::dumpEngineParameters() {
	URHO3D_LOGINFO("[ParameterDumper]::{dumpEngineParameters()} begin");
	logParameter(EP_AUTOLOAD_PATHS);
	logParameter(EP_BORDERLESS);
	logParameter(EP_DUMP_SHADERS);
	logParameter(EP_EVENT_PROFILER);
	logParameter(EP_EXTERNAL_WINDOW);
	logParameter(EP_FLUSH_GPU);
	logParameter(EP_FORCE_GL2);
	logParameter(EP_FRAME_LIMITER);
	logParameter(EP_FULL_SCREEN);
	logParameter(EP_HEADLESS);
	logParameter(EP_HIGH_DPI);
	logParameter(EP_LOG_LEVEL);
	logParameter(EP_LOG_NAME);
	logParameter(EP_LOG_QUIET);
	logParameter(EP_LOW_QUALITY_SHADOWS);
	logParameter(EP_MATERIAL_QUALITY);
	logParameter(EP_MONITOR);
	logParameter(EP_MULTI_SAMPLE);
	logParameter(EP_ORIENTATIONS);
	logParameter(EP_PACKAGE_CACHE_DIR);
	logParameter(EP_RENDER_PATH);
	logParameter(EP_REFRESH_RATE);
	logParameter(EP_RESOURCE_PACKAGES);
	logParameter(EP_RESOURCE_PATHS);
	logParameter(EP_RESOURCE_PREFIX_PATHS);
	logParameter(EP_SHADER_CACHE_DIR);
	logParameter(EP_SHADOWS);
	logParameter(EP_SOUND);
	logParameter(EP_SOUND_BUFFER);
	logParameter(EP_SOUND_INTERPOLATION);
	logParameter(EP_SOUND_MIX_RATE);
	logParameter(EP_SOUND_STEREO);
	logParameter(EP_TEXTURE_ANISOTROPY);
	logParameter(EP_TEXTURE_FILTER_MODE);
	logParameter(EP_TEXTURE_QUALITY);
	logParameter(EP_TIME_OUT);
	logParameter(EP_TOUCH_EMULATION);
	logParameter(EP_TRIPLE_BUFFER);
	logParameter(EP_VSYNC);
	logParameter(EP_WINDOW_HEIGHT);
	logParameter(EP_WINDOW_ICON);
	logParameter(EP_WINDOW_POSITION_X);
	logParameter(EP_WINDOW_POSITION_Y);
	logParameter(EP_WINDOW_RESIZABLE);
	logParameter(EP_WINDOW_TITLE);
	logParameter(EP_WINDOW_WIDTH);
	logParameter(EP_WORKER_THREADS);
	URHO3D_LOGINFO("[ParameterDumper]::{dumpEngineParameters()} end");
}

void ParameterDumper::logParameter(String parameterName) {
	const Variant& variant = parameters_[parameterName];
	String type = "none";
	String value = variant.ToString();
	VariantType variantType = variant.GetType();
	
	switch(variantType) {
		case VAR_NONE:  break;
		case VAR_INT: type = "int"; break;
		case VAR_BOOL: type = "bool"; break;
		case VAR_FLOAT: type = "float"; break;
		case VAR_STRING: type = "string"; break;
		case VAR_DOUBLE: type = "int"; break;
		default: type = "unknown"; break;
	}

	String message = formatParameterInfo(parameterName, variant, type);
	URHO3D_LOGINFO(message);
}

String ParameterDumper::formatParameterInfo(String parameterName, const Variant& parameter, String type) {
	String message;
	if ( showType_) {
		message.Append(type);
		message.Append(" ");
	}

	message.Append(parameterName);
	message.Append(" = ");
	message.Append(parameter.ToString());
	return message;
}