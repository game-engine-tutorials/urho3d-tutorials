#include "EnvironmentSettings.h"
#include "ParameterDumper.h"

using namespace Urho3D;

EnvironmentSettings::EnvironmentSettings(Context * context) 
: Application(context) {
}


void EnvironmentSettings::Setup() {
    URHO3D_LOGINFO("[EnvironmentSettings]::{Setup()} begin");
    #ifdef __DEBUG__ // Symbol '__DEBUG__' is usually defined when running cmake tool
    engineParameters_[EP_FULL_SCREEN]=false;  // (Urho3D::)EP_FULL_SCREEN = "Fullscreen"
    engineParameters_[EP_WINDOW_RESIZABLE]=true;
    #else
    engineParameters_[EP_FULL_SCREEN] = false;  // If we compile for release, then we can display in fullscreen
    #endif

    // Configuration not depending whether we compile for debug or release.
    engineParameters_[EP_WINDOW_WIDTH]=1280;
    engineParameters_[EP_WINDOW_HEIGHT]=720;

    // All 'EP_' constants are defined in ${URHO3D_INSTALL}/include/Urho3D/Engine/EngineDefs.h file    
    URHO3D_LOGINFO("[EnvironmentSettings]::[Setup()} end");
}


void EnvironmentSettings::Start() {
    URHO3D_LOGINFO("[EnvironmentSettings]::{Start()} begin");
	ParameterDumper dumper(engineParameters_);
	dumper.dumpEngineParameters();
	engine_->Exit();
	
	URHO3D_LOGINFO("[EnvironmentSettings]::{Start()} end");
}

void EnvironmentSettings::Stop(){
    URHO3D_LOGINFO("[EnvironmentSettings]::{Stop()} begin");
    
    URHO3D_LOGINFO("[EnvironmentSettings]::{Start()} end");
}


