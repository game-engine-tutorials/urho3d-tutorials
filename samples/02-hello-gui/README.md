This is the second of the tutorials that come with Urho3D. It shows how to create a simple dialog window within the game scene. You find it normally in the source tree, under the Source/Samples folder. The sample can be compiled and executed out of the source tree. This is important, because as soon as you start to modify the original tutorial you need to be able to have several versions of it. 

Note that you need to execute some preliminary steps like for my other tutorials. The ../include/Sample.h and ../src/Sample.cpp files must be copied into the include / src folders.

Note that the tutorials 08 and 08 from ther warming-up part are based on this sample.

Happy coding!