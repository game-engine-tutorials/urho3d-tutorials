This is the third of the tutorials that come with Urho3D. It shows how to create a window with some sprites that dance on screen. It coule be used as a screesaver. You find it normally in the source tree, under the Source/Samples folder. 

Note that you need to execute some preliminary steps like for my other tutorials. The ../include/Sample.h and ../src/Sample.cpp files must be copied into the include / src folders.

Happy coding!