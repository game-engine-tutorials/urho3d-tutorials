#pragma once

#include <Urho3D/Urho3DAll.h>

class MutantField : public Urho3D::Object
{
	URHO3D_OBJECT(MutantField, Urho3D::Object);
public:
	explicit MutantField(Urho3D::Context* context);
	MutantField& scene(Urho3D::Scene* theScene);
	MutantField& bounds(Urho3D::BoundingBox theBoundingBox);
	MutantField& model(Urho3D::String path);
	MutantField& material(Urho3D::String path);
	MutantField& animation(Urho3D::String path);
	void createMutant();

private:
	void createNode();
	void createModel();
	void createMaterial();
	void createAnimation();

	Urho3D::SharedPtr<Urho3D::Scene> scene_;
	Urho3D::SharedPtr<Urho3D::Node> mutantNode;
	Urho3D::SharedPtr<Urho3D::AnimatedModel> mutantModel;
	Urho3D::SharedPtr<Urho3D::Material> mutantMaterial;

	Urho3D::BoundingBox bounds_;
	
	Urho3D::String pathToModel;
	Urho3D::String pathToMaterial;
	Urho3D::String pathToAnimation;
};