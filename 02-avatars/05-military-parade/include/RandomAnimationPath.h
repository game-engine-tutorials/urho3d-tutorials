#pragma once

#include <Urho3D/Urho3DAll.h>

class RandomAnimationPath
{
public:
	static Urho3D::String randomPath();
};