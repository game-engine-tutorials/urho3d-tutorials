#pragma once

#include <Urho3D/Urho3DAll.h>


class FieldDivider
{
public:
	FieldDivider();
	FieldDivider& min(Urho3D::Vector2 newMinCoordinates);
	FieldDivider& max(Urho3D::Vector2 newMaxCoordinates);
	FieldDivider& rows(unsigned int newRows);
	FieldDivider& columns(unsigned int newColumns);

	unsigned int rows();
	unsigned int columns();

	BoundingBox division(unsigned int row, unsigned int column);

private:
	Urho3D::Vector2 min_;
	Urho3D::Vector2 max_;
	unsigned int rows_;
	unsigned int columns_;


};