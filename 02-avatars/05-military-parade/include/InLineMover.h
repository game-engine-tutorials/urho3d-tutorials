#pragma once

#include <Urho3D/Urho3DAll.h>


class InLineMover : public Urho3D::LogicComponent
{
    URHO3D_OBJECT(InLineMover, Urho3D::LogicComponent);

public:
    explicit InLineMover(Urho3D::Context* context);

    //void SetParameters(float moveSpeed, const BoundingBox& bounds, Urho3D::SharedPtr<Urho3D::Node> lightSpot, bool backMove);
    void SetParameters(float moveSpeed, const BoundingBox& bounds,  bool backMove);

    void Update(float timeStep) override;
    float GetMoveSpeed() const { return moveSpeed_; }
    const BoundingBox& GetBounds() const { return bounds_; }

private:
    float moveSpeed_;
    Urho3D::BoundingBox bounds_;
    Urho3D::SharedPtr<Urho3D::Node> lightSpotNode;
    bool backwardsMove; // Some character animations are inverted 

    void updateAvatarPosition(float timeStep);
    void updateLightSpotPosition(float timeStep);
    bool detectEndOftheRoad();
    void turnAvater180Degrees();
    void turnLightSpot180Degrees();
};