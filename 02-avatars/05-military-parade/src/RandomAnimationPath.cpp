#include "RandomAnimationPath.h"

using namespace Urho3D;




String RandomAnimationPath::randomPath()
{
	int pathIndex = Random(2000);
	pathIndex = pathIndex%13;

	switch(pathIndex)
	{
		case 0:
			return "Models/Mutant/Mutant_HipHop1.ani";
		case 1:
			return "Models/Mutant/Mutant_Idle1.ani";
		case 2:
			return "Models/Mutant/Mutant_Jump.ani";
		case 3:
			return "Models/Mutant/Mutant_JumpStop.ani";
		case 4:
			return "Models/Mutant/Mutant_Run.ani";
		case 5:
			return "Models/Mutant/Mutant_Walk.ani";
		case 6:
			return "Models/Mutant/Mutant_Death.ani";
		case 7:
			return "Models/Mutant/Mutant_Idle0.ani";
		case 8:
			return "Models/Mutant/Mutant_Jump1.ani";
		case 9:
			return "Models/Mutant/Mutant_JumpAttack.ani";
		case 10:
			return "Models/Mutant/Mutant_Kick.ani";
		case 11:
			return "Models/Mutant/Mutant_Punch.ani";
		case 12:
			return "Models/Mutant/Mutant_Swipe.ani";

		default:
			break;
	}
	return "";
}

