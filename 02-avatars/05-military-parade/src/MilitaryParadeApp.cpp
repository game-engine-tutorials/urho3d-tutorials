// This is based on sample no. 5 from Urho source tree
//
#include "MilitaryParadeApp.h"
#include "InLineMover.h"
#include "FieldDivider.h"
#include "RandomAnimationPath.h"




#include <iostream>
#include <cstdlib>


using namespace Urho3D;
using namespace std;

const char EXTRA_GRAPHIC_RESOURCE_PATH[] = "XGRPATH";
const float DIMENSIONS_EXPRESSED_IN_CENTIMETERS = 1.0f;
const float MODEL_MOVE_SPEED = 2.0f;
const float AXIAL_DISTANCE_LIMIT = 25.0f; // the border of the plane is 50.0f
const float AXIAL_CLOSE_PROXIITY = 3.0f; // the border of the plane is 50.0f

// Uncomment the next line if ypou want a longer walk/run of the avatar
//const BoundingBox bounds(Vector3(-AXIAL_DISTANCE_LIMIT, 0.0f, -AXIAL_DISTANCE_LIMIT), Vector3(AXIAL_DISTANCE_LIMIT, 0.0f, AXIAL_DISTANCE_LIMIT));
const BoundingBox bounds(Vector3(-AXIAL_CLOSE_PROXIITY, 0.0f, -AXIAL_DISTANCE_LIMIT), Vector3(AXIAL_DISTANCE_LIMIT, 0.0f, AXIAL_CLOSE_PROXIITY));

MilitaryParadeApp::MilitaryParadeApp(Context* context)
: Application(context) {
	errorsDetected = false; 
    context->RegisterFactory<InLineMover>();
}

void MilitaryParadeApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Moves an avatar on the screen - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void MilitaryParadeApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(MilitaryParadeApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());
    char* additionalResourceDir = getenv(EXTRA_GRAPHIC_RESOURCE_PATH);
    if ( additionalResourceDir != NULL) 
        cache->AddResourceDir(additionalResourceDir);

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void MilitaryParadeApp::Stop() {
	engine_->DumpResources(true);
}

void MilitaryParadeApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void MilitaryParadeApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(MilitaryParadeApp, HandleUpdate));
}

void MilitaryParadeApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();
    moveCamera(timeStep);
}

void MilitaryParadeApp::createScene()
{
    
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();
    createGroupOfAvatars();
}

void MilitaryParadeApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();

    planeNode = scene_->CreateChild("Ground");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void MilitaryParadeApp::createLightSource() {
   
    lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
}



Node* MilitaryParadeApp::createPointLightSource(Color color, Vector3 position) {
    Node* lightNode = scene_->CreateChild("PointLight");
    lightNode->SetPosition(position);
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_POINT);
    light->SetRange(10.0f);
    light->SetColor(color);
    light->SetBrightness(3.0f);
    return lightNode;
}


void MilitaryParadeApp::createCamera() {

    yaw_ = -180.0f;   // horizontal angle
    pitch_ = 0.0f; // vertical angle

    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 1.5f,4.0f));
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
}

void MilitaryParadeApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move camera. ");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}

void MilitaryParadeApp::createGroupOfAvatars()
{

    FieldDivider fieldDivider;
    fieldDivider.rows(10).columns(10);
    for ( int row = 0; row < fieldDivider.rows(); ++row){
        for (int column = 0; column < fieldDivider.columns(); ++column ){
            BoundingBox bounds = fieldDivider.division(row, column);
            SharedPtr<MutantField> mutantField(new MutantField(context_));
            (*mutantField)
                .scene(scene_)
                .bounds(bounds)
                .model("Models/Mutant/Mutant.mdl")
                .material("Models/Mutant/Materials/mutant_M.xml")
                .animation(RandomAnimationPath::randomPath())
                .createMutant();


            mutantFields.Push(mutantField);
        }
    }
}

void MilitaryParadeApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void MilitaryParadeApp::moveCamera(float timeStep)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

}





    