#include "InLineMover.h"

using namespace Urho3D;

InLineMover::InLineMover(Context* context) :
    LogicComponent(context),
    moveSpeed_(0.0f),
    backwardsMove(false)
{
    SetUpdateEventMask(USE_UPDATE);
}
/*
void InLineMover::SetParameters(float moveSpeed,  const BoundingBox& bounds, SharedPtr<Node> lightSpot, bool backMove)
{
    moveSpeed_ = moveSpeed;
    bounds_ = bounds;
    lightSpotNode = lightSpot;
    backwardsMove = backMove;
}
*/
void InLineMover::SetParameters(float moveSpeed,  const BoundingBox& bounds, bool backMove)
{
    moveSpeed_ = moveSpeed;
    bounds_ = bounds;
    backwardsMove = backMove;
}

void InLineMover::Update(float timeStep)
{
    updateAvatarPosition(timeStep);
    //updateLightSpotPosition(timeStep);

    if (detectEndOftheRoad())
    {
        turnAvater180Degrees();
        //turnLightSpot180Degrees();
    }

    

    auto* model = node_->GetComponent<AnimatedModel>(true);
    if (model->GetNumAnimationStates())
    {
        AnimationState* state = model->GetAnimationStates()[0];
        state->AddTime(timeStep);
    }
}

void InLineMover::updateAvatarPosition(float timeStep)
{
    node_->Translate(Vector3::FORWARD * moveSpeed_ * timeStep);
}

void InLineMover::updateLightSpotPosition(float timeStep)
{
    if ( backwardsMove)
        lightSpotNode->Translate(-Vector3::FORWARD * moveSpeed_ * timeStep);
    else
        lightSpotNode->Translate(Vector3::FORWARD * moveSpeed_ * timeStep);
}

bool InLineMover::detectEndOftheRoad()
{
    Vector3 pos = node_->GetPosition();
    if (    pos.x_ < bounds_.min_.x_ 
        ||  pos.x_ > bounds_.max_.x_ 
        ||  pos.z_ < bounds_.min_.z_ 
        ||  pos.z_ > bounds_.max_.z_)
        return true;
    return false;
}

void InLineMover::turnAvater180Degrees()
{
    node_->Yaw(180.0f);
}

void InLineMover::turnLightSpot180Degrees()
{
    lightSpotNode->Yaw(180.0f);
}
