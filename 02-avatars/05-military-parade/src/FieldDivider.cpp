#include "FieldDivider.h"

using namespace Urho3D;

FieldDivider::FieldDivider()
: 
	min_(-50.0f,-50.0f),
	max_(50.0f,50.0f),
	rows_(1),
	columns_(1)

{

}

FieldDivider& FieldDivider::min(Vector2 newMinCoordinates)
{
	min_ = newMinCoordinates;
	return *this;
}

FieldDivider& FieldDivider::max(Vector2 newMaxCoordinates)
{
	max_ = newMaxCoordinates;
	return *this;
}

FieldDivider& FieldDivider::rows(unsigned int newRows)
{
	rows_ = newRows;
	return *this;
}

FieldDivider& FieldDivider::columns(unsigned int newColumns)
{
	columns_ = newColumns;
	return *this;
}

unsigned int FieldDivider::rows()
{
	return rows_;
}

unsigned int FieldDivider::columns()
{
	return columns_;
}

// this could be precalculated, but for teaching sake ...
BoundingBox FieldDivider::division(unsigned int row, unsigned int column)
{
	float totalRowLength = max_.x_ - min_.x_;
	float totalColumnLength = max_.y_ - min_.y_;

	float rowLength = totalRowLength / rows_;
	float columnLength = totalColumnLength / columns_;

	float xMin = min_.x_ + row * rowLength;
	float xMax = xMin + rowLength;

	float zMin = min_.y_ + column * columnLength;
	float zMax = zMin + columnLength;

	BoundingBox theBox( Vector3(xMin, 0.0, zMin), Vector3(xMax, 0.0f, zMax));
	return theBox;
}

