#include "MutantField.h"
#include "InLineMover.h"
#include <iostream>

using namespace Urho3D;
using namespace std;

const float DIMENSIONS_EXPRESSED_IN_CENTIMETERS = 1.0f;
const float MODEL_MOVE_SPEED = 2.0f;

MutantField::MutantField(Urho3D::Context* context)
: Object(context)
{


}


MutantField& MutantField::scene(Scene* theScene)
{
	scene_ = theScene;
	return *this;
}

MutantField& MutantField::bounds(Urho3D::BoundingBox theBoundingBox)
{
	bounds_ = theBoundingBox;
	return *this;
}

MutantField& MutantField::model(Urho3D::String path)
{
	pathToModel = path;
	return *this;
}

MutantField& MutantField::material(Urho3D::String path)
{
	pathToMaterial = path;
	return *this;
}

MutantField& MutantField::animation(Urho3D::String path)
{
	pathToAnimation = path;
	return *this;
}

void MutantField::createMutant()
{
	createNode();
	createModel();
	createMaterial();
	createAnimation();
}

void MutantField::createNode()
{
	Vector3 center = bounds_.Center();

	mutantNode = scene_->CreateChild("Mutant");
    mutantNode->SetPosition(center);
    mutantNode->SetRotation(Quaternion(0.0f, 180.0f, 0.0f));
    mutantNode->SetWorldScale(DIMENSIONS_EXPRESSED_IN_CENTIMETERS);
}

void MutantField::createModel()
{
	auto* cache = GetSubsystem<ResourceCache>();
	mutantModel = mutantNode->CreateComponent<AnimatedModel>();
    mutantModel->SetModel(cache->GetResource<Model>(pathToModel));
    mutantModel->SetCastShadows(true);
}

void MutantField::createMaterial()
{
	auto* cache = GetSubsystem<ResourceCache>();
 	mutantMaterial = cache->GetResource<Material>(pathToMaterial);
    mutantModel->SetMaterial(mutantMaterial);
}

void MutantField::createAnimation()
{
	
	auto* cache = GetSubsystem<ResourceCache>();
	auto* animation = cache->GetResource<Animation>(pathToAnimation);

    AnimationState* state = mutantModel->AddAnimationState(animation);
    if (state)
    {
        state->SetWeight(1.0f);
        state->SetLooped(true);
        state->SetTime(Random(animation->GetLength()));
    } else {
        cout << "Failed to load animation" << endl;
    }

    auto* mover = mutantNode->CreateComponent<InLineMover>();
    mover->SetParameters(-MODEL_MOVE_SPEED,  bounds_, true);

}
