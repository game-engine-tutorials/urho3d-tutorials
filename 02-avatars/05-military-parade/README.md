This is yet another tutorial based heavily on sample no. 06 from Urho tree code. There is a catch.

The original examplee used the same movement of the character again and again. There are 100 mutants (10 rows of 10 ) and each one of them is executing one among a dozen different movements like walk, run, attack, jump.

For sake of clatrity, I divided the field of view in as many individual areas as the mutants. You can see that they always limit themselves to a specific part of the plane. There are no comments in the code, but the name of the classes are quite explicit.

You can play with the number of mutants on screen. The FieldDivider class distribute them uniformly across the ground. While I tested a 30x30 case successfully, I believe a monster game should not have more than a dozen simultaneously on screen.

Some animations seem erroneous. The LogicComponent class that move the monsters on screen doesn't take into consideration that some characters don't move in a horizontal plane. The idle mutants should not move. I leave to you to change that. After all, this is just a tutorial.

Happy coding!