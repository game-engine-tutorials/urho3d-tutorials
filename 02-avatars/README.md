This folder contains code related to game avatars / characters. This is an important part of any game. Until now, everything was possible with code. Avatars are 3D models of characters that can move in the game world.

Designing a good avatar takes time. So far, the models from mixamo are not fully compatible with Urho. The animations are behaving strangely adn the texture colors mix up in a funny way. The only case that is working is from sample no. 06 and I will start with it. When I'll put in place an automatic pipeline that converts mixamo models into Urho models at an acceptable level, you'll see the code related to those models.

While the sample no. 06 does several things, I prefer to cut it in pieces:

- loading a 3D model
- adding textures
- in-place walking
- scene walking (automatic)
- camera following the walking character
- driving the character with the arrow keys

After that, I will continue to look into how to use other models. One thing I noticed is that by default, the mixamo character is very tall. Apparenty it is an issue of units. Some 3D modeling software programs use centimeters, while other ones use meters. This explains why my first character I used was as tall as some well known statue in Asia.