#include "SceneLightTool.h"


using namespace Urho3D;

SceneLightTool::SceneLightTool()
: scene_(nullptr),
  color_(1.0f,1.0f,1.0f),
  direction_(0.0f,10.0f,0.0f),
  name_("unknown"),
  position_(0.0f,0.0f,0.0f)
{

}

SceneLightTool& SceneLightTool::scene(Urho3D::SharedPtr<Urho3D::Scene> updatedScene)
{
	scene_ = updatedScene;
	return *this;
}

SceneLightTool& SceneLightTool::color(Urho3D::Color updatedColor)
{
	color_ = updatedColor;
	return *this;
}

SceneLightTool& SceneLightTool::direction(Urho3D::Vector3 updatedDirection)
{
	direction_ = updatedDirection;
	return *this;
}

SceneLightTool& SceneLightTool::name(Urho3D::String updatedName)
{
	name_ = updatedName;
	return *this;
}

SceneLightTool& SceneLightTool::position(Urho3D::Vector3 updatedPosition)
{
	position_ = updatedPosition;
	return *this;
}

Node* SceneLightTool::createDirectionalLight()
{
	Node* node = scene_->CreateChild(name_);
    node->SetDirection(direction_); 

    Light* light = node->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);

    return node;
}

Node* SceneLightTool::createPointLight() 
{
	Node* node = scene_->CreateChild(name_);
    node->SetPosition(position_);

    Light* light = node->CreateComponent<Light>();
    light->SetLightType(LIGHT_POINT);
    light->SetRange(10.0f);
    light->SetColor(color_);
    light->SetBrightness(1.0f);
    return node;
}