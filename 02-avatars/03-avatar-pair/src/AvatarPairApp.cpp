// This is based on sample no. 5 from Urho source tree
//
#include "AvatarPairApp.h"
#include "SceneLightTool.h"


#include <iostream>
#include <cstdlib>


using namespace Urho3D;
using namespace std;

const char EXTRA_GRAPHIC_RESOURCE_PATH[] = "XGRPATH";
const float DIMENSIONS_EXPRESSED_IN_CENTIMETERS = 1.0f;

AvatarPairApp::AvatarPairApp(Context* context)
: Application(context) {
	errorsDetected = false; 

}

void AvatarPairApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Applies a texture to an avatar model - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void AvatarPairApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(AvatarPairApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());
    char* additionalResourceDir = getenv(EXTRA_GRAPHIC_RESOURCE_PATH);
    if ( additionalResourceDir != NULL) 
        cache->AddResourceDir(additionalResourceDir);

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void AvatarPairApp::Stop() {
	engine_->DumpResources(true);
}

void AvatarPairApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void AvatarPairApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(AvatarPairApp, HandleUpdate));
}

void AvatarPairApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    moveCamera(timeStep);

}

void AvatarPairApp::createScene()
{
    
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSources();
    createCamera();

    createCharacter();
    

}

void AvatarPairApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();

    planeNode = scene_->CreateChild("Ground");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void AvatarPairApp::createLightSources() 
{
    createAmbientLight();
    createHumanPointLight();
    createMutantPointLight();
}


void AvatarPairApp::createAmbientLight()
{
    SceneLightTool tool;
    tool.scene(scene_);
    ambientLight = tool
        .name("DirectionalLight")
        .direction(Vector3(0.6f, -1.0f, 0.8f))
        .createDirectionalLight();
}

void AvatarPairApp::createHumanPointLight()
{
    SceneLightTool tool;
    tool.scene(scene_);
    humanPointLight = tool
        .name("HumanPointLight")
        .position(Vector3(0.0f, 4.0f, 3.0f))
        .color(Color(1.0f,1.0f,0.0f))
        .createPointLight();
}

void AvatarPairApp::createMutantPointLight()
{
    SceneLightTool tool;
    tool.scene(scene_);
    mutantPointLight = tool
        .name("MutantPointLight")
        .position(Vector3(3.0f, 4.0f, 3.0f))
        .color(Color(1.0f,0.0f,1.0f))
        .createPointLight();
}

void AvatarPairApp::createCamera() {

    yaw_ = -200.0f;   // horizontal angle
    pitch_ = 0.0f; // vertical angle

    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 1.5f,4.0f));
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
}

void AvatarPairApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move. ");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}

void AvatarPairApp::createHuman()
{
    auto* cache = GetSubsystem<ResourceCache>();

    humanNode = scene_->CreateChild("Avatar");
    humanNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
    humanNode->SetRotation(Quaternion(0.0f, 70.0f, 0.0f));
    humanNode->SetWorldScale(DIMENSIONS_EXPRESSED_IN_CENTIMETERS);

    humanModel = humanNode->CreateComponent<AnimatedModel>();
    humanModel->SetModel(cache->GetResource<Model>("Models/Kachujin/Kachujin.mdl"));
    humanModel->SetCastShadows(true);

    humanMaterial = cache->GetResource<Material>("Models/Kachujin/Materials/Kachujin.xml");
    humanModel->SetMaterial(humanMaterial);
}

void AvatarPairApp::createMutant()
{
    auto* cache = GetSubsystem<ResourceCache>();

    mutantNode = scene_->CreateChild("Avatar");
    mutantNode->SetPosition(Vector3(3.0f, 0.0f, 0.0f));
    mutantNode->SetRotation(Quaternion(0.0f, 130.0f, 0.0f));
    mutantNode->SetWorldScale(DIMENSIONS_EXPRESSED_IN_CENTIMETERS);

    mutantModel = mutantNode->CreateComponent<AnimatedModel>();
    mutantModel->SetModel(cache->GetResource<Model>("Models/Mutant/Mutant.mdl"));
    mutantModel->SetCastShadows(true);

    mutantMaterial = cache->GetResource<Material>("Models/Mutant/Materials/mutant_M.xml");
    mutantModel->SetMaterial(mutantMaterial);
}

void AvatarPairApp::createCharacter()
{
   createHuman();
   createMutant();
}

void AvatarPairApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void AvatarPairApp::moveCamera(float timeStep)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

}





    