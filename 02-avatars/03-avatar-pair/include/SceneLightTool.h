#pragma once

#include <Urho3D/Urho3DAll.h>

class SceneLightTool 
{
public:
	SceneLightTool();
	Urho3D::Node* createDirectionalLight();
	Urho3D::Node* createPointLight();

	SceneLightTool& scene(Urho3D::SharedPtr<Urho3D::Scene> updatedScene);
	SceneLightTool& color(Urho3D::Color updatedColor);
	SceneLightTool& direction(Urho3D::Vector3 updatedDirection);
	SceneLightTool& name(Urho3D::String updatedName);
	SceneLightTool& position(Urho3D::Vector3 updatedPosition);

private:
	Urho3D::SharedPtr<Urho3D::Scene> scene_;
	Urho3D::Color color_;
	Urho3D::Vector3 direction_;
	Urho3D::String name_;
	Urho3D::Vector3 position_;
};

