#pragma once

#include <Urho3D/Urho3DAll.h>

class AvatarPairApp : public Urho3D::Application {
	URHO3D_OBJECT(AvatarPairApp, Urho3D::Application);
public:
	explicit AvatarPairApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSources();
    void createCamera();
    void createInstructions();
    void createCharacter();

    void createAmbientLight();
    void createHumanPointLight();
    void createMutantPointLight();

	void createHuman();
	void createMutant();

    
    void setupViewport();
    void moveCamera(float timeStep);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;

    Urho3D::SharedPtr<Node> ambientLight;
    

	Urho3D::SharedPtr<Node> humanPointLight;
    Urho3D::SharedPtr<Node> humanNode;
    Urho3D::SharedPtr<AnimatedModel> humanModel;
    Urho3D::SharedPtr<Material> humanMaterial;

	Urho3D::SharedPtr<Node> mutantPointLight;
    Urho3D::SharedPtr<Node> mutantNode;
    Urho3D::SharedPtr<AnimatedModel> mutantModel;
    Urho3D::SharedPtr<Material> mutantMaterial;
    
};