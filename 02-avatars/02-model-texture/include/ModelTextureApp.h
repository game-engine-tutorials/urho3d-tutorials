#pragma once

#include <Urho3D/Urho3DAll.h>

class ModelTextureApp : public Urho3D::Application {
	URHO3D_OBJECT(ModelTextureApp, Urho3D::Application);
public:
	explicit ModelTextureApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSource();
    void createCamera();
    void createInstructions();
    void createCharacter();
    Urho3D::Node* createPointLightSource(Urho3D::Color color, Urho3D::Vector3 position);

    
    void setupViewport();
    void moveCamera(float timeStep);

    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;
    Urho3D::SharedPtr<Node> lightNode;
    Urho3D::SharedPtr<Node> lightSpotNode;


    Urho3D::SharedPtr<Node> avatarNode;
    Urho3D::SharedPtr<AnimatedModel> avatarModel;
    Urho3D::SharedPtr<Material> avatarMaterial;

    
    
};