In this tutorial we apply a texture to a character already loaded. It is recommended to do the model loading tutorial before. Overall, it is a simplified version of sample no. 06

Note that the character now has a texture.

For sake of visibility, I added a spot light above the avatar and a bit in front of it. 
