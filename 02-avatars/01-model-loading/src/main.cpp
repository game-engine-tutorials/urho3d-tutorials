#include <Urho3D/Urho3DAll.h>
#include "ModelLoadingApp.h"


int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Urho3D::SharedPtr<ModelLoadingApp> application(new ModelLoadingApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}