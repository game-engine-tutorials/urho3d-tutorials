This is yet another tutorial based heavily on sample no. 06 from Urho tree code. 

When you play an RPG, there is a good change you see the character from behind (3rd persin view). Then, by using the arrow keys, you move the avatar in the virtual world.

I believe there is an extra sample that deals with character movement. So far, I'm using only information from sample no. 6.

Note how the avatar moves the legs. This is in sync with the position in the horizontal plane. I added the division by 10 in ordrr to have a smooth animation. Feel free to change the values.

There is no control on the position of the avatar. He can move outside the lit square plane. Again, this tutorial is focusing on howto move the avatar.

Happy coding!