Add proper rotation. This is 2D geometry so that camera follows the mutant from behind whenever  it rotates

For the mutant:

	if (left)
        avatarNode->Rotate(Quaternion(0.0f, -1.0f, 0.0f));
    else if (right)
        avatarNode->Rotate(Quaternion(0.0f, 1.0f, 0.0f));

For the camera:


	if (left)
    	cameraNode_->Rotate(Quaternion(0.0f, -1.0f, 0.0f));
    if (right)
        cameraNode_->Rotate(Quaternion(0.0f, +1.0f, 0.0f));

Plus some translations  