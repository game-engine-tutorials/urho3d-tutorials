#pragma once

#include <Urho3D/Urho3DAll.h>

class AvatarDirectorApp : public Urho3D::Application {
	URHO3D_OBJECT(AvatarDirectorApp, Urho3D::Application);
public:
	explicit AvatarDirectorApp(Urho3D::Context* context);
private:
	void Setup() override;
	void Start() override;
	void Stop() override;

	void HandleKeyUp(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);


    void createScene();
    void createPlane();
    void createLightSource();
    void createCamera();
    void createInstructions();
    void createAvatar();

    
    void setupViewport();
    void moveCamera(float timeStep, bool forward, bool backwards, bool left, bool right);
    void moveAvatar(float timeStep, bool forward, bool backwards, bool left, bool right);


    bool errorsDetected;
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    float yaw_;
    float pitch_;

    Urho3D::SharedPtr<Node> planeNode;
    Urho3D::SharedPtr<StaticModel> planeObject;
    Urho3D::SharedPtr<Node> lightNode;
    Urho3D::SharedPtr<Node> lightSpotNode;


    Urho3D::SharedPtr<Node> avatarNode;
    Urho3D::SharedPtr<AnimatedModel> avatarModel;
    Urho3D::SharedPtr<Material> avatarMaterial;

    
    
};