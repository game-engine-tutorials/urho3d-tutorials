// This is based on sample no. 6 from Urho source tree
//
#include "AvatarDirectorApp.h"



#include <iostream>
#include <cstdlib>


using namespace Urho3D;
using namespace std;

const char EXTRA_GRAPHIC_RESOURCE_PATH[] = "XGRPATH";
const float DIMENSIONS_EXPRESSED_IN_CENTIMETERS = 1.0f;
const float MODEL_MOVE_SPEED = 2.0f;



AvatarDirectorApp::AvatarDirectorApp(Context* context)
: Application(context) {
	errorsDetected = false; 
}

void AvatarDirectorApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Moves an avatar on the screen - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void AvatarDirectorApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(AvatarDirectorApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());
    char* additionalResourceDir = getenv(EXTRA_GRAPHIC_RESOURCE_PATH);
    if ( additionalResourceDir != NULL) 
        cache->AddResourceDir(additionalResourceDir);

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void AvatarDirectorApp::Stop() {
	engine_->DumpResources(true);
}

void AvatarDirectorApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void AvatarDirectorApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(AvatarDirectorApp, HandleUpdate));
}

void AvatarDirectorApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    if (GetSubsystem<UI>()->GetFocusElement())
            return;

    auto* input = GetSubsystem<Input>();
    bool goForward = false;
    bool goBackwards = false;
    bool goLeft = false;
    bool goRight = false;

    if (input->GetKeyDown(KEY_UP))
        goForward = true;
    if (input->GetKeyDown(KEY_DOWN))
        goBackwards = true;
    if (input->GetKeyDown(KEY_LEFT))
        goLeft = true;
    if (input->GetKeyDown(KEY_RIGHT))
        goRight = true;

    moveAvatar(timeStep,goForward, goBackwards, goLeft, goRight);
    moveCamera(timeStep,goForward, goBackwards, goLeft, goRight);
}

void AvatarDirectorApp::createScene()
{
    
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();
    createAvatar();
}

void AvatarDirectorApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();

    planeNode = scene_->CreateChild("Ground");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void AvatarDirectorApp::createLightSource() {
   
    lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
}





void AvatarDirectorApp::createCamera() {

    yaw_ = -180.0f;   // horizontal angle
    pitch_ = 0.0f; // vertical angle

    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 1.5f,4.0f));
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
}

void AvatarDirectorApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys to move the mutant. ");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void AvatarDirectorApp::createAvatar()
{
    auto* cache = GetSubsystem<ResourceCache>();

    avatarNode = scene_->CreateChild("Avatar");
    avatarNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
    avatarNode->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
    avatarNode->SetWorldScale(DIMENSIONS_EXPRESSED_IN_CENTIMETERS);

    avatarModel = avatarNode->CreateComponent<AnimatedModel>();
    avatarModel->SetModel(cache->GetResource<Model>("Models/Mutant/Mutant.mdl"));

    avatarModel->SetCastShadows(true);

    avatarMaterial = cache->GetResource<Material>("Models/Mutant/Materials/mutant_M.xml");

    avatarModel->SetMaterial(avatarMaterial);

    auto* walkAnimation = cache->GetResource<Animation>("Models/Mutant/Mutant_Walk.ani");
    auto* runAnimation = cache->GetResource<Animation>("Models/Mutant/Mutant_Run.ani");

    // You can change the following line and choose the other animation
    auto* animation = runAnimation;

    AnimationState* state = avatarModel->AddAnimationState(animation);
    if (state)
    {
        state->SetWeight(1.0f);
        state->SetLooped(true);
        state->SetTime(Random(animation->GetLength()));
    } else {
        cout << "Failed to load animation" << endl;
    }


}

void AvatarDirectorApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}


void AvatarDirectorApp::moveAvatar(float timeStep, bool forward, bool backwards, bool left, bool right)
{
    if (GetSubsystem<UI>()->GetFocusElement())
            return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;

    if (forward)
        avatarNode->Translate(-Vector3::FORWARD * MOVE_SPEED * timeStep);
    else if (backwards)
        avatarNode->Translate(-Vector3::BACK * MOVE_SPEED * timeStep);
    else if (left)
        avatarNode->Translate(-Vector3::LEFT * MOVE_SPEED * timeStep);
    else if (right)
        avatarNode->Translate(-Vector3::RIGHT * MOVE_SPEED * timeStep);

    auto* model = avatarNode->GetComponent<AnimatedModel>(true);
    if (model->GetNumAnimationStates())
    {
        AnimationState* state = model->GetAnimationStates()[0];

        if (forward || right)
            state->AddTime(-MOVE_SPEED *timeStep / 10.0f );
        else if (backwards || left)
            state->AddTime(MOVE_SPEED *timeStep / 10.0f);
    }

}

void AvatarDirectorApp::moveCamera(float timeStep, bool forward, bool backwards, bool left, bool right)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;


    const float MOVE_SPEED = 20.0f;


    
    if (forward)
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (backwards)
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (left)
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (right)
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);



    
    
}





    