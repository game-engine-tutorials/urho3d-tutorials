This tutorial is based on sample no. 06 from the Urho source tree. I replaced the human avatar with the mutant. There are fare more animations available. 

Note a few things:

- the avatar is moving
- there is a spot light following the avatar
- the scene is lit onmy by the moving light above the avatar
- the presence of the backwards flag
- the detection of the end of the road

Moving

The .ani file resources contain all you need in order to animate a character. YOu load the model, you put a texture and now you direct the avatar to follow a choreography. As there are half a dozen .ani files for this mutant, it is interesting to see how it moves.

Light

Until now, the light has been fixed. Remember that all light sources are asssociated to a node. If you move the node, you move the light.  It is quite easy to put a flashlight in the hand of the avatar. For now, I put it abve his head.

Darkness

In order to better appreciate the moving light, I turned off the ambient light. You can turn it on if you want. See the code from the precedent tutorial.

Backwards flag

I started with the character from the sample no. 06. The lady was moving forward. After changing several resourcees, I got a mutant moving ... backwards. I believe there is an internal orientation flag that makes it possible. ANyway, in order to obtain a natural movement, I added a backwards flag. As you can see, now the mutant looks quite realistic.

End of the road

In order to keep things in order, the mutant has to move within the limits of the plane (100 x 100 unit square centered in the point 0;0). This is where the bounding box comes into play. The Logic Component associated with the character detects the proximity of the border and turns the avatar 180 degrees

Happy Coding ! 