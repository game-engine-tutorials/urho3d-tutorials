#include <Urho3D/Urho3DAll.h>
#include "AvatarMoveApp.h"


int runApplication()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Urho3D::SharedPtr<AvatarMoveApp> application(new AvatarMoveApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}