// This is based on sample no. 5 from Urho source tree
//
#include "AvatarMoveApp.h"
#include "InLineMover.h"



#include <iostream>
#include <cstdlib>


using namespace Urho3D;
using namespace std;

const char EXTRA_GRAPHIC_RESOURCE_PATH[] = "XGRPATH";
const float DIMENSIONS_EXPRESSED_IN_CENTIMETERS = 1.0f;
const float MODEL_MOVE_SPEED = 2.0f;
const float AXIAL_DISTANCE_LIMIT = 25.0f; // the border of the plane is 50.0f
const float AXIAL_CLOSE_PROXIITY = 3.0f; // the border of the plane is 50.0f

// Uncomment the next line if ypou want a longer walk/run of the avatar
//const BoundingBox bounds(Vector3(-AXIAL_DISTANCE_LIMIT, 0.0f, -AXIAL_DISTANCE_LIMIT), Vector3(AXIAL_DISTANCE_LIMIT, 0.0f, AXIAL_DISTANCE_LIMIT));
const BoundingBox bounds(Vector3(-AXIAL_CLOSE_PROXIITY, 0.0f, -AXIAL_DISTANCE_LIMIT), Vector3(AXIAL_DISTANCE_LIMIT, 0.0f, AXIAL_CLOSE_PROXIITY));

AvatarMoveApp::AvatarMoveApp(Context* context)
: Application(context) {
	errorsDetected = false; 
    context->RegisterFactory<InLineMover>();
}

void AvatarMoveApp::Setup() {
	engineParameters_[EP_WINDOW_TITLE] = "Moves an avatar on the screen - press ESC to close"; 
    engineParameters_[EP_FULL_SCREEN]  = false; 

	if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
    {
    	std::cerr 	<< std::endl 
    				<< "You have to set the environment variable EP_RESOURCE_PREFIX_PATHS before launching the executable" 
    				<< std::endl 
    				<< "The program will halt soon"
    				<< std::endl;
    	errorsDetected = true;
    	engine_->Exit(); // graceful shutdown
    }
}

void AvatarMoveApp::Start() {
	if (!errorsDetected)
	    	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(AvatarMoveApp, HandleKeyUp));

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	cache->AddResourceDir(GetSubsystem<FileSystem>()->GetProgramDir());
    char* additionalResourceDir = getenv(EXTRA_GRAPHIC_RESOURCE_PATH);
    if ( additionalResourceDir != NULL) 
        cache->AddResourceDir(additionalResourceDir);

    createScene();
    createInstructions();
    setupViewport();

    SubscribeToEvents();
}

void AvatarMoveApp::Stop() {
	engine_->DumpResources(true);
}

void AvatarMoveApp::HandleKeyUp(StringHash eventType, VariantMap& eventData) {
	using namespace KeyUp;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESCAPE)
    	engine_->Exit();
}

void AvatarMoveApp::SubscribeToEvents()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(AvatarMoveApp, HandleUpdate));
}

void AvatarMoveApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();
    moveCamera(timeStep);
}

void AvatarMoveApp::createScene()
{
    
    auto* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    createPlane();
    createLightSource();
    createCamera();
    createAvatar();
}

void AvatarMoveApp::createPlane() {
    auto* cache = GetSubsystem<ResourceCache>();

    planeNode = scene_->CreateChild("Ground");
    planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
    planeObject = planeNode->CreateComponent<StaticModel>();
    planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
    planeObject->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
}

void AvatarMoveApp::createLightSource() {
   
    lightSpotNode = createPointLightSource(Color(1.0f,1.0f,1.0f), Vector3(0.0f, 3.0f, 0.0f));
}


Node* AvatarMoveApp::createPointLightSource(Color color, Vector3 position) {
    Node* lightNode = scene_->CreateChild("PointLight");
    lightNode->SetPosition(position);
    auto* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_POINT);
    light->SetRange(6.0f);
    light->SetColor(color);
    light->SetBrightness(1.0f);
    return lightNode;
}


void AvatarMoveApp::createCamera() {

    yaw_ = -180.0f;   // horizontal angle
    pitch_ = 0.0f; // vertical angle

    cameraNode_ = scene_->CreateChild("Fritz Lang");
    cameraNode_->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 1.5f,4.0f));
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
}

void AvatarMoveApp::createInstructions()
{
    auto* cache = GetSubsystem<ResourceCache>();
    auto* ui = GetSubsystem<UI>();

    // Construct new Text object, set string to display and font to use
    auto* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Use arrow ( UP, DOWN, LEFT, RIGHT ) keys and mouse/touch to move camera. ");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_TOP);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}


void AvatarMoveApp::createAvatar()
{
    auto* cache = GetSubsystem<ResourceCache>();

    avatarNode = scene_->CreateChild("Avatar");
    avatarNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
    avatarNode->SetRotation(Quaternion(0.0f, 180.0f, 0.0f));
    avatarNode->SetWorldScale(DIMENSIONS_EXPRESSED_IN_CENTIMETERS);

    avatarModel = avatarNode->CreateComponent<AnimatedModel>();
    avatarModel->SetModel(cache->GetResource<Model>("Models/Mutant/Mutant.mdl"));

    avatarModel->SetCastShadows(true);

    avatarMaterial = cache->GetResource<Material>("Models/Mutant/Materials/mutant_M.xml");

    avatarModel->SetMaterial(avatarMaterial);

    auto* walkAnimation = cache->GetResource<Animation>("Models/Mutant/Mutant_Walk.ani");
    auto* runAnimation = cache->GetResource<Animation>("Models/Mutant/Mutant_Run.ani");

    // You can change the following line and choose the other animation
    auto* animation = runAnimation;

    AnimationState* state = avatarModel->AddAnimationState(animation);
    if (state)
    {
        state->SetWeight(1.0f);
        state->SetLooped(true);
        state->SetTime(Random(animation->GetLength()));
    } else {
        cout << "Failed to load animation" << endl;
    }

    auto* mover = avatarNode->CreateComponent<InLineMover>();
    mover->SetParameters(-MODEL_MOVE_SPEED,  bounds, lightSpotNode, true);
}

void AvatarMoveApp::setupViewport()
{
    auto* renderer = GetSubsystem<Renderer>();
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void AvatarMoveApp::moveCamera(float timeStep)
{
    if (GetSubsystem<UI>()->GetFocusElement())
        return;

    auto* input = GetSubsystem<Input>();

    const float MOVE_SPEED = 20.0f;
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // The four arrow keys move the camera
    if (input->GetKeyDown(KEY_UP))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_DOWN))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_LEFT))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown(KEY_RIGHT))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

}





    