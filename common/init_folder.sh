#!/bin/bash

# Now you can add more than one root resource dir. The CoreData and Data folders are no longer mandatory
mkdir -p bin
#mkdir -p bin/Data
#mkdir -p bin/CoreData
mkdir -p src
mkdir -p include


ln -s ../../common/CMake CMake
