This tutorial analyzes the orientation of a triangular face with respect to a point. the normal vector is outwards, i.e. on the other side of the plane of the triangle than the point.

T!he idea here is that for a convex polyhedron, given a point inside the solid body, all the normal vectors should point outwards. The clockwise algorithm needs a referential and this would be the inside point. 

Don't expect so see graphics. This is just a bunch of geometry formulas. I plan to use them later for programatic generation of complex models out of simpler ones. This tutorial requires some pondering so I'll do several commits. 

The README file will be updated according to the most recent commit. So far there is just the skeleton app.