#include <Urho3D/Urho3DAll.h>
#include "NormalVectorApp.h"

using namespace Urho3D;

int runApplication()
{
	SharedPtr<Context> context(new Context());
	SharedPtr<NormalVectorApp> application(new NormalVectorApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}