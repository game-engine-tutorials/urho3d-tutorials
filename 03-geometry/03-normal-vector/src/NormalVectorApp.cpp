// Based on the sample no. 34 from the Urho source tree

#include "NormalVectorApp.h"

#include <iostream>

using namespace Urho3D;
using namespace std;


NormalVectorApp::NormalVectorApp(Context * context) 
: Application(context) {
}

void NormalVectorApp::Setup() {
    //URHO3D_LOGINFO("[LoadAndSaveApp]::{Setup()} begin");

    // No graphic window
    engineParameters_[EP_HEADLESS] = true;

}

void NormalVectorApp::Start() {

	engine_->Exit();
}

void NormalVectorApp::Stop(){    
}





