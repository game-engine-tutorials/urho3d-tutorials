#pragma once

#include <Urho3D/Urho3DAll.h>
class TetrahedronData;
class TetrahedronApp : public Urho3D::Application {
public:
	TetrahedronApp(Urho3D::Context *context);
	/*virtual*/ void Setup() override;
	/*virtual*/ void Start() override;
	/*virtual*/	void Stop() override;
	
private:
	
	bool saveModel(Urho3D::SharedPtr<Urho3D::Model> model, Urho3D::String resModelFilePath);
	Urho3D::SharedPtr<Model> createTetrahedronModel();
	Urho3D::BoundingBox calcSmallestBox(TetrahedronData& data);
};

