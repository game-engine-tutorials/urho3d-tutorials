#pragma once
#include "TetrahedronFace.h"

class TetrahedronData
{
public:
	TetrahedronData();
	TetrahedronFace& face(unsigned index);
	unsigned faceCount();
	
private:
	void init();
	TetrahedronFace f0_;
	TetrahedronFace f1_;
	TetrahedronFace f2_;
	TetrahedronFace f3_;


};