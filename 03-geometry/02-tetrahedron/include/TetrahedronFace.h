#pragma once


#include <Urho3D/Urho3DAll.h>

// no move ctor/operator here
class TetrahedronFace
{
public:
	TetrahedronFace();
	TetrahedronFace(const TetrahedronFace& other);
	TetrahedronFace(Urho3D::Vector3& v1, Urho3D::Vector3& v2, Urho3D::Vector3& v3);
	TetrahedronFace& operator =(const TetrahedronFace& other);

	Urho3D::Vector3& v1();
	Urho3D::Vector3& v2();
	Urho3D::Vector3& v3();
	TetrahedronFace& v1(Urho3D::Vector3& value);
	TetrahedronFace& v2(Urho3D::Vector3& value);
	TetrahedronFace& v3(Urho3D::Vector3& value);

	Urho3D::Vector3& n1();
	Urho3D::Vector3& n2();
	Urho3D::Vector3& n3();

	Urho3D::Vector3& flipNormal();


	TetrahedronFace& recalcNormalVectors();
	TetrahedronFace& sync();
	float* vertexData();
	unsigned vertexDataSize();

	float minX();
	float minY();
	float minZ();
	float maxX();
	float maxY();
	float maxZ();
private:

	Urho3D::Vector3 v1_;
	Urho3D::Vector3 v2_;
	Urho3D::Vector3 v3_;
	Urho3D::Vector3 n1_;
	Urho3D::Vector3 n2_;
	Urho3D::Vector3 n3_;

	float vertexData_[18];

};