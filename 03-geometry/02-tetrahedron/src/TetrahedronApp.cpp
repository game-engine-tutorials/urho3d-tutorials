// Based on the sample no. 34 from the Urho source tree

#include "TetrahedronApp.h"
#include "TetrahedronData.h"

#include <iostream>

using namespace Urho3D;
using namespace std;


TetrahedronApp::TetrahedronApp(Context * context) 
: Application(context) {
}

void TetrahedronApp::Setup() {
    //URHO3D_LOGINFO("[LoadAndSaveApp]::{Setup()} begin");

    // No graphic window
    engineParameters_[EP_HEADLESS] = true;

}

void TetrahedronApp::Start() {


    SharedPtr<Model> model = createTetrahedronModel();
    saveModel(model, "Tetrahedron.mdl");
	engine_->Exit();
	
}

void TetrahedronApp::Stop(){    
}



bool TetrahedronApp::saveModel(SharedPtr<Model> model, String resModelFilePath)
{
    File fileOut(context_);
    bool success = false;

    if (fileOut.Open(resModelFilePath, FILE_WRITE))
    {
        model->Save(fileOut);
        fileOut.Flush();
        fileOut.Close();
        success = true;
    }

    return success;
}


BoundingBox TetrahedronApp::calcSmallestBox(TetrahedronData& data)
{
    float minX, maxX;
    float minY, maxY;
    float minZ, maxZ;

    TetrahedronFace& aFace = data.face(0);
    minX = aFace.minX();
    minY = aFace.minY();
    minZ = aFace.minZ();

    maxX = aFace.maxX();
    maxY = aFace.maxY();
    maxZ = aFace.maxZ();

    for ( unsigned faceID = 1; faceID < data.faceCount(); ++faceID)
    {

        TetrahedronFace& face = data.face(faceID);
        float minXCandidate = face.minX();
        float minYCandidate = face.minY();
        float minZCandidate = face.minZ();

        float maxXCandidate = face.maxX();
        float maxYCandidate = face.maxY();
        float maxZCandidate = face.maxZ();

        if ( minX > minXCandidate) minX = minXCandidate;
        if ( minY > minYCandidate) minY = minYCandidate;
        if ( minZ > minZCandidate) minZ = minZCandidate;

        if ( maxX > maxXCandidate) maxX = maxXCandidate;
        if ( maxY > maxYCandidate) maxY = maxYCandidate;
        if ( maxZ > maxZCandidate) maxZ = maxZCandidate;
    }

    return BoundingBox(Vector3(minX, minY, minZ), Vector3(maxX, maxY, maxZ));
}

SharedPtr<Model> TetrahedronApp::createTetrahedronModel()
{
    // calls init automatically
    TetrahedronData tetrahedronData;
    TetrahedronFace& aFace = tetrahedronData.face(0);

    unsigned faceVertexDataSize = aFace.vertexDataSize();

    unsigned vertexDataSize = faceVertexDataSize * tetrahedronData.faceCount();

    float *vertexData = new float[vertexDataSize];
    unsigned vertexDataPos = 0;

    for ( unsigned faceID = 0; faceID < tetrahedronData.faceCount(); ++faceID)
    {

        TetrahedronFace& face = tetrahedronData.face(faceID);
        float* faceVertexData = face.vertexData();

        for ( unsigned faceVertexDataPos =0; faceVertexDataPos <faceVertexDataSize;  )
        {
            vertexData[vertexDataPos++] = faceVertexData[faceVertexDataPos++];
        }
    }

    unsigned faceVertexCount = 3; // a face is a triangle so 3 vertices
    unsigned vertexCount = faceVertexCount * tetrahedronData.faceCount();
    unsigned short *indexData = new unsigned short[vertexCount];


    for ( unsigned vertexPos = 0; vertexPos < vertexCount; ++vertexPos)
    {
        indexData[vertexPos] = vertexPos;
    }

    SharedPtr<Model> tetrahedronModel(new Model(context_));
    SharedPtr<VertexBuffer> vb(new VertexBuffer(context_));
    SharedPtr<IndexBuffer> ib(new IndexBuffer(context_));
    SharedPtr<Geometry> geom(new Geometry(context_));

    vb->SetShadowed(true);
    PODVector<VertexElement> elements;
    elements.Push(VertexElement(TYPE_VECTOR3, SEM_POSITION));
    elements.Push(VertexElement(TYPE_VECTOR3, SEM_NORMAL));
    vb->SetSize(vertexCount, elements);
    vb->SetData(vertexData);

    ib->SetShadowed(true);
    ib->SetSize(vertexCount, false);
    ib->SetData(indexData);

    geom->SetVertexBuffer(0, vb);
    geom->SetIndexBuffer(ib);
    geom->SetDrawRange(TRIANGLE_LIST, 0, vertexCount);


    tetrahedronModel->SetNumGeometries(1);
    tetrahedronModel->SetGeometry(0, 0, geom);

    tetrahedronModel->SetBoundingBox(calcSmallestBox(tetrahedronData));

    Vector<SharedPtr<VertexBuffer> > vertexBuffers;
    Vector<SharedPtr<IndexBuffer> > indexBuffers;
    vertexBuffers.Push(vb);
    indexBuffers.Push(ib);

    PODVector<unsigned> morphRangeStarts;
    PODVector<unsigned> morphRangeCounts;
    morphRangeStarts.Push(0);
    morphRangeCounts.Push(0);
    tetrahedronModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
    tetrahedronModel->SetIndexBuffers(indexBuffers);


    return tetrahedronModel;
}


