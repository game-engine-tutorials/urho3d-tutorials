#include <cmath>
#include <Urho3D/Urho3DAll.h>
#include "TetrahedronData.h"

#include <iostream>
using namespace Urho3D;

using namespace std;
TetrahedronData::TetrahedronData()
{
	init();
}



TetrahedronFace& TetrahedronData::face(unsigned index)
{
	switch(index){
		case 0: return f0_;
		case 1: return f1_;
		case 2: return f2_;
		case 3: return f3_;

		default: 
			cout  << " ERRORRRRRRRRR =================================================" << endl; 
		return f1_;
	}
}


unsigned TetrahedronData::faceCount()
{
	return 4;
}

void TetrahedronData::init()
{
	// see https://en.wikipedia.org/wiki/Tetrahedron for this



	Vector3 ptA(0.0f,1.0f, 0.0f);
	Vector3 ptO(-sqrt(2.0f / 9.0f),-1.0f / 3.0f, sqrt(2.0f / 3.0f));
	Vector3 ptC(-sqrt(2.0f / 9.0f), -1.0f / 3.0f, -sqrt(2.0f / 3.0f));
	Vector3 ptB(sqrt(8.0f/9.0f), -1.0f/3.0f, 0.0f);

	// The vertical axis is Oy, the horizontal plane is made by axes Ox and  Oz
	// A is the upper vertex, while B,O,C are in the same horizontal plane. 

	//The order of the face vertices: 
	// AOB, ACO, ABC, CBO
	// that would be clockwise, clockwise, clockwise, clockwise

	f0_.v1(ptA).v2(ptO).v3(ptB).recalcNormalVectors().sync();
	f1_.v1(ptA).v2(ptC).v3(ptO).recalcNormalVectors().sync();
	f2_.v1(ptA).v2(ptB).v3(ptC).recalcNormalVectors().sync();
	f3_.v1(ptC).v2(ptB).v3(ptO).recalcNormalVectors().sync();


}



