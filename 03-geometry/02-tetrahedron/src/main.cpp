#include <Urho3D/Urho3DAll.h>
#include "TetrahedronApp.h"

using namespace Urho3D;

int runApplication()
{
	SharedPtr<Context> context(new Context());
	SharedPtr<TetrahedronApp> application(new TetrahedronApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}