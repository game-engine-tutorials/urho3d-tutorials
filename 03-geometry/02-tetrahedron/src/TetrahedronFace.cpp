#include "TetrahedronFace.h"
#include <iostream>
using namespace Urho3D;
using namespace std;
TetrahedronFace::TetrahedronFace()
{

}

TetrahedronFace::TetrahedronFace(const TetrahedronFace& other)
{
	v1_ = other.v1_;
	v2_ = other.v2_;
	v3_ = other.v3_;

}

TetrahedronFace::TetrahedronFace(Vector3& v1, Vector3& v2, Vector3& v3)
:
	v1_(v1),
	v2_(v2),
	v3_(v3)
{

}


TetrahedronFace& TetrahedronFace::operator =(const TetrahedronFace& other)
{
	v1_ = other.v1_;
	v2_ = other.v2_;
	v3_ = other.v3_;
	return *this;
}

Vector3& TetrahedronFace::v1()
{
	return v1_;
}

Vector3& TetrahedronFace::v2()
{
	return v2_;
}

Vector3& TetrahedronFace::v3()
{
	return v3_;
}

TetrahedronFace& TetrahedronFace::v1(Vector3& value)
{
	v1_ = value;
	return *this;
}

TetrahedronFace& TetrahedronFace::v2(Vector3& value)
{
	v2_ = value;
	return *this;
}

TetrahedronFace& TetrahedronFace::v3(Vector3& value)
{
	v3_ = value;
	return *this;
}

Vector3& TetrahedronFace::n1()
{
	return n1_;
}

Vector3& TetrahedronFace::n2()
{
	return n2_;
}

Vector3& TetrahedronFace::n3()
{
	return n3_;
}


Urho3D::Vector3& TetrahedronFace::flipNormal()
{
	n1_ *= -1;
	n2_ *= -1;
	n3_ *= -1;
}

TetrahedronFace& TetrahedronFace::recalcNormalVectors()
{
	cout << "V1 = [" << v1_.x_ << ";"<<v1_.y_ << ";" << v1_.z_  << "]" << endl;
	cout << "V2 = [" << v2_.x_ << ";"<<v2_.y_ << ";" << v2_.z_ <<  "]" << endl;
	cout << "V3 = [" << v3_.x_ << ";"<<v3_.y_ << ";" << v3_.z_  << "]" << endl;

	Vector3 edge1 = v1_ - v2_;
	cout << "E1 = [" << edge1.x_ << ";"<<edge1.y_ << ";" << edge1.z_  << "]" << endl;

    Vector3 edge2 = v1_ - v3_;
    cout << "E2 = [" << edge2.x_ << ";"<<edge2.y_ << ";" << edge2.z_   << "]" << endl;


    n1_ = n2_ = n3_ = -edge1.CrossProduct(edge2).Normalized();
	cout << "N1 = [" << n1_.x_ << ";"<<n1_.y_ << ";" << n1_.z_   << "]" << endl;

    return *this;
}

TetrahedronFace& TetrahedronFace::sync()
{
	int pos = 0;

	vertexData_[pos++]=v1_.x_;
	vertexData_[pos++]=v1_.y_;
	vertexData_[pos++]=v1_.z_;

	vertexData_[pos++]=n1_.x_;
	vertexData_[pos++]=n1_.y_;
	vertexData_[pos++]=n1_.z_;

	vertexData_[pos++]=v2_.x_;
	vertexData_[pos++]=v2_.y_;
	vertexData_[pos++]=v2_.z_;

	vertexData_[pos++]=n2_.x_;
	vertexData_[pos++]=n2_.y_;
	vertexData_[pos++]=n2_.z_;

	vertexData_[pos++]=v3_.x_;
	vertexData_[pos++]=v3_.y_;
	vertexData_[pos++]=v3_.z_;

	vertexData_[pos++]=n3_.x_;
	vertexData_[pos++]=n3_.y_;
	vertexData_[pos++]=n3_.z_;

	return *this;
}

float* TetrahedronFace::vertexData()
{
	return vertexData_;
}

unsigned TetrahedronFace::vertexDataSize()
{
	return 18;
}


float TetrahedronFace::minX()
{
	float x12 = v1_.x_ < v2_.x_ ? v1_.x_ : v2_.x_;
	float x13 = v1_.x_ < v3_.x_ ? v1_.x_ : v3_.x_;
	return x12 < x13 ? x12 : x13;
}

float TetrahedronFace::minY()
{
	float y12 = v1_.y_ < v2_.y_ ? v1_.y_ : v2_.y_;
	float y13 = v1_.y_ < v3_.y_ ? v1_.y_ : v3_.y_;
	return y12 < y13 ? y12 : y13;
}

float TetrahedronFace::minZ()
{
	float z12 = v1_.z_ < v2_.z_ ? v1_.z_ : v2_.z_;
	float z13 = v1_.z_ < v3_.z_ ? v1_.z_ : v3_.z_;
	return z12 < z13 ? z12 : z13;
}

float TetrahedronFace::maxX()
{
	float x12 = v1_.x_ > v2_.x_ ? v1_.x_ : v2_.x_;
	float x13 = v1_.x_ > v3_.x_ ? v1_.x_ : v3_.x_;
	return x12 > x13 ? x12 : x13;
}

float TetrahedronFace::maxY()
{
	float y12 = v1_.y_ > v2_.y_ ? v1_.y_ : v2_.y_;
	float y13 = v1_.y_ > v3_.y_ ? v1_.y_ : v3_.y_;
	return y12 > y13 ? y12 : y13;
}

float TetrahedronFace::maxZ()
{
	float z12 = v1_.z_ > v2_.z_ ? v1_.z_ : v2_.z_;
	float z13 = v1_.z_ > v3_.z_ ? v1_.z_ : v3_.z_;
	return z12 > z13 ? z12 : z13;
}
