This example is based on sample no. 34 from the Urho source tree.

I simplified the polihedron as much as possible. The square pyramid becomes a tetrahedron. While this tutorial generates a model file for a regular tetrahedron, the normals to the faces are still determined by the vertex order. In a future tutporial I plan to use the convexity of the polyhedron in order to get the right sequence.
Until then, use the small pyramid from this example in your projects.

Happy coding!