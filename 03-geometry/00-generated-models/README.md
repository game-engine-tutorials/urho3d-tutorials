This folder contains some models that don't come with Urho. I don't know how to use Blender, and this is a fact. Best alternative for me is the generation of models through code. I know, it is slower and the learning curve is sharp. 

The list starts with the simplest regular polyhedron, the tetrahedron.