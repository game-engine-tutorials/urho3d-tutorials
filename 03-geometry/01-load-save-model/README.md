This tutorial shows how to save a model in a file. While Urho comes with a plethora of models and textures, one size doesn't fit all. One could solve the problem by creating a model in code and then saving it in a file. This example will be followed by some more practical cases.

Happy coding!