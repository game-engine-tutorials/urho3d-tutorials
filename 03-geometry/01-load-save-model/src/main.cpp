#include <Urho3D/Urho3DAll.h>
#include "LoadAndSaveApp.h"

using namespace Urho3D;

int runApplication()
{
	SharedPtr<Context> context(new Context());
	SharedPtr<LoadAndSaveApp> application(new LoadAndSaveApp(context));
	return application->Run();
}

int main(int argc, char** argv)
{
	Urho3D::ParseArguments(argc, argv);
	return runApplication();
}