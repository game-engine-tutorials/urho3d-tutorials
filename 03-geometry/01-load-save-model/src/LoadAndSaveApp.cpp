#include "LoadAndSaveApp.h"

using namespace Urho3D;

LoadAndSaveApp::LoadAndSaveApp(Context * context) 
: Application(context) {
}

void LoadAndSaveApp::Setup() {
    URHO3D_LOGINFO("[LoadAndSaveApp]::{Setup()} begin");

    // No graphic window
    engineParameters_[EP_HEADLESS] = true;

    URHO3D_LOGINFO("[LoadAndSaveApp]::[Setup()} end");
}

void LoadAndSaveApp::Start() {
    URHO3D_LOGINFO("[LoadAndSaveApp]::{Start()} begin");

	loadAndSaveModel("Models/Box.mdl", "Perfection.mdl");
	engine_->Exit();
	
    URHO3D_LOGINFO("[LoadAndSaveApp]::{Start()} end");
}

void LoadAndSaveApp::Stop(){
    URHO3D_LOGINFO("[LoadAndSaveApp]::{Stop()} begin");
    
    URHO3D_LOGINFO("[LoadAndSaveApp]::{Start()} end");
}

SharedPtr<Model> LoadAndSaveApp::loadModel(String resModelFilePath)
{
    URHO3D_LOGINFO("[LoadAndSaveApp]::{loadModel()} begin");
    
    SharedPtr<ResourceCache> cache(GetSubsystem<ResourceCache>());
    SharedPtr<Model> model (cache->GetResource<Model>(resModelFilePath));
    
    URHO3D_LOGINFO("[LoadAndSaveApp]::{loadModel()} end");
    
    return model;
}


bool LoadAndSaveApp::saveModel(SharedPtr<Model> model, String resModelFilePath)
{
    URHO3D_LOGINFO("[LoadAndSaveApp]::{saveModel()} begin");
    File fileOut(context_);
    bool success = false;

    if (fileOut.Open(resModelFilePath, FILE_WRITE))
    {
        model->Save(fileOut);
        fileOut.Flush();
        fileOut.Close();
        success = true;
    }

    URHO3D_LOGINFO("[LoadAndSaveApp]::{saveModel()} end");
    return success;
}

bool LoadAndSaveApp::loadAndSaveModel(String inModelFilePath, String outModelFilePath)
{
    URHO3D_LOGINFO("[LoadAndSaveApp]::{loadAndSaveModel()} begin");

    SharedPtr<Context> context(new Context());
    bool success = true;
    // First load a model
    SharedPtr<Model> model = loadModel(inModelFilePath);
    if (!model){
        success = false;
    } else {
        if (!saveModel(model, outModelFilePath)){
            success = false;
        }
    }
    URHO3D_LOGINFO("[LoadAndSaveApp]::{loadAndSaveModel()} end");

    return success;
}
