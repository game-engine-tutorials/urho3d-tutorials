#pragma once

#include <Urho3D/Urho3DAll.h>

class LoadAndSaveApp : public Urho3D::Application {
public:
	LoadAndSaveApp(Urho3D::Context *context);
	/*virtual*/ void Setup() override;
	/*virtual*/ void Start() override;
	/*virtual*/	void Stop() override;
	
private:
	Urho3D::SharedPtr<Urho3D::Model> loadModel(Urho3D::String resModelFilePath);
	bool saveModel(Urho3D::SharedPtr<Urho3D::Model> model, Urho3D::String resModelFilePath);
	bool loadAndSaveModel(Urho3D::String inModelFilePath, Urho3D::String outModelFilePath);
};

