# urho3d-tutorials

A step-by-step tutorial at a much slower pace than the samples that come with Urho3D.

A few remarks about the folder structure. 

1. The 1vank-samples folder contains code from https://github.com/1vanK. I only made small changes in order to make it compilable under this repo.
2. The samples folder contains the original samples from the Urho3D source tree. The only change I made is about the Sample.inl file, in order to allow an out of surce tree compilation. You will find on gitlab or github some repos where it is stated that you need to copy it over the Urho3D source code. I don't quite like this. Urho is meant to be installed somewhere on your computer, after which you shouldn't mess with it. Or simply you might not have access to the source tree. 
3. The tutorials are numbered as NN-something/MM-something-else where NN and MM are 2-digit numbers. So far there is a warming up stage. I tried to make the examples a bit easier than the original tutorials. It is known that Urho has a sharp learning curve. I disagree with it. You only need to slow doan the pace.
4. The common folder is the only part that somewhat replicates the compiling system of Urho. If you know a simpler way to do it, let me know. So far, it seems to work for me.

Happy learning and coding ! By the way, I'm learning it too.