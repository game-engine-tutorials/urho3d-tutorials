﻿#pragma once
#include <Urho3D/Urho3DAll.h>


class Game : public Urho3D::Application
{
    URHO3D_OBJECT(Game, Urho3D::Application);

public:
    Game(Urho3D::Context* context);

    virtual void Setup();
    virtual void Start();

    void ClearBoard();
    void CreateBoard();



protected:
    void SetLogoVisible(bool enable);
    Urho3D::SharedPtr<Urho3D::Scene> scene_;
    Urho3D::SharedPtr<Urho3D::Node> cameraNode_;
    Urho3D::SharedPtr<Urho3D::Node> lightNode_;
    Vector3 cameraTarget_;
    IntVector2 RandomEmptyCell();
    void CreateBall(BallColor color, int x, int y);
    void CreateBall(BallColor color, IntVector2 cell);
    Vector3 CellToWorld(int x, int y);
    void CreateButton3D(Vector3 pos);

private:
    void CreateScene();
    void SetupViewport();
    void MoveCamera(float timeStep);
    void SubscribeToEvents();
    void HandleUpdate(Urho3D::StringHash eventType, VariantMap& eventData);
    void HandlePostRenderUpdate(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    bool Raycast(float maxDistance, Urho3D::Vector3& hitPos, Urho3D::Drawable*& hitDrawable);
    void CreateUI();
    void CreateUIWindow();
    Window* window_;
    void HandleClosePressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleExitPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleEasyPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleNormalPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleHardPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleExpertPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);
    void HandleMasterPressed(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);

    Node* boardNode_;
};
