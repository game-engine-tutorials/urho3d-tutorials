While looking for examples of 2D games built with Urho I came across this GitHub repository: https://github.com/1vanK/Urho3D-Color-Lines. The final result looks nice. However, after cloning the code from the repository, it didn't compile and I had a dozen errors. 

It is already stated in the readmy of the original repo that the code might not compile. As the screen capture looked great, I made some small changes to the code and now it compiles. Of course, you need to call the scripts like for any other of the tutorials from this repo (see the warming part).

The game is pretty decent and it looks even better in full screen. 

I'm not sure if the resource files under bin are all necessary, but I added them as well. I plan to remove in the future the unnecessary parts.

Thank you 1vanK. Btw, the Flappy Bird game is also fantastic. I'll put the code later here.
